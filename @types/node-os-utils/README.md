# Installation
> `npm install --save @types/node-os-utils`

# Summary
This package contains type definitions for node-os-utils (https://github.com/SunilWang/node-os-utils).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node-os-utils.

### Additional Details
 * Last updated: Tue, 31 May 2022 18:31:36 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Nasreddine Bac Ali](https://github.com/bacali95), and [Sasial](https://github.com/sasial-dev).
