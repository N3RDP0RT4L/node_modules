# Installation
> `npm install --save @types/notp`

# Summary
This package contains type definitions for notp (https://github.com/guyht/notp).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/notp.

### Additional Details
 * Last updated: Mon, 14 Mar 2022 22:01:49 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [DefinitelyTyped](https://github.com/DefinitelyTyped).
