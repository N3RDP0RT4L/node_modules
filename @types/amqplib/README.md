# Installation
> `npm install --save @types/amqplib`

# Summary
This package contains type definitions for amqplib (https://github.com/squaremo/amqp.node).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/amqplib.

### Additional Details
 * Last updated: Sat, 21 Aug 2021 01:31:21 GMT
 * Dependencies: [@types/bluebird](https://npmjs.com/package/@types/bluebird), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Michael Nahkies](https://github.com/mnahkies), [Ab Reitsma](https://github.com/abreits), [Nicolás Fantone](https://github.com/nfantone), [Nick Zelei](https://github.com/nickzelei), [Vincenzo Chianese](https://github.com/XVincentX), and [Seonggwon Yoon](https://github.com/seonggwonyoon).
