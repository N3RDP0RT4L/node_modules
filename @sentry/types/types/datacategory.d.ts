export declare type DataCategory = 'default' | 'error' | 'transaction' | 'security' | 'attachment' | 'session' | 'internal';
//# sourceMappingURL=datacategory.d.ts.map