import { StackFrame } from './stackframe';
/** JSDoc */
export interface Stacktrace {
    frames?: StackFrame[];
    frames_omitted?: [number, number];
}
export declare type StackParser = (stack: string, skipFirst?: number) => StackFrame[];
export declare type StackLineParserFn = (line: string) => StackFrame | undefined;
export declare type StackLineParser = [number, StackLineParserFn];
//# sourceMappingURL=stacktrace.d.ts.map