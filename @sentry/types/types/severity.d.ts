/**
 * @deprecated Please use a `SeverityLevel` string instead of the `Severity` enum. Acceptable values are 'fatal',
 * 'error', 'warning', 'log', 'info', and 'debug'.
 */
export declare enum Severity {
    /** JSDoc */
    Fatal = "fatal",
    /** JSDoc */
    Error = "error",
    /** JSDoc */
    Warning = "warning",
    /** JSDoc */
    Log = "log",
    /** JSDoc */
    Info = "info",
    /** JSDoc */
    Debug = "debug"
}
export declare type SeverityLevel = 'fatal' | 'error' | 'warning' | 'log' | 'info' | 'debug';
//# sourceMappingURL=severity.d.ts.map