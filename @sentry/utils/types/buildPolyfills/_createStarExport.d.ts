import { GenericObject } from './types';
/**
 * Copy properties from an object into `exports`.
 *
 * Adapted from Sucrase (https://github.com/alangpierce/sucrase)
 *
 * @param obj The object containing the properties to copy.
 */
export declare function _createStarExport(obj: GenericObject): void;
//# sourceMappingURL=_createStarExport.d.ts.map