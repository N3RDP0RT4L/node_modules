import { RequireResult } from './types';
/**
 * Wraps modules which aren't the result of transpiling an ESM module in an object under the key `default`
 *
 * Adapted from Sucrase (https://github.com/alangpierce/sucrase)
 *
 * @param requireResult The result of calling `require` on a module
 * @returns `requireResult` or `requireResult` wrapped in an object, keyed as `default`
 */
export declare function _interopRequireDefault(requireResult: RequireResult): RequireResult;
//# sourceMappingURL=_interopRequireDefault.d.ts.map