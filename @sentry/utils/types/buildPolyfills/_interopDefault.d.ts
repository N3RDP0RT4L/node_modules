import { RequireResult } from './types';
/**
 * Unwraps a module if it has been wrapped in an object under the key `default`.
 *
 * Adapted from Rollup (https://github.com/rollup/rollup)
 *
 * @param requireResult The result of calling `require` on a module
 * @returns The full module, unwrapped if necessary.
 */
export declare function _interopDefault(requireResult: RequireResult): RequireResult;
//# sourceMappingURL=_interopDefault.d.ts.map