import { Hub } from '@sentry/core';
import { Context, DynamicSamplingContext, MeasurementUnit, Transaction as TransactionInterface, TransactionContext, TransactionMetadata } from '@sentry/types';
import { Span as SpanClass } from './span';
/** JSDoc */
export declare class Transaction extends SpanClass implements TransactionInterface {
    metadata: TransactionMetadata;
    /**
     * The reference to the current hub.
     */
    readonly _hub: Hub;
    private _name;
    private _measurements;
    private _contexts;
    private _trimEnd?;
    private _frozenDynamicSamplingContext;
    /**
     * This constructor should never be called manually. Those instrumenting tracing should use
     * `Sentry.startTransaction()`, and internal methods should use `hub.startTransaction()`.
     * @internal
     * @hideconstructor
     * @hidden
     */
    constructor(transactionContext: TransactionContext, hub?: Hub);
    /** Getter for `name` property */
    get name(): string;
    /** Setter for `name` property, which also sets `source` as custom */
    set name(newName: string);
    /**
     * JSDoc
     */
    setName(name: string, source?: TransactionMetadata['source']): void;
    /**
     * Attaches SpanRecorder to the span itself
     * @param maxlen maximum number of spans that can be recorded
     */
    initSpanRecorder(maxlen?: number): void;
    /**
     * @inheritDoc
     */
    setContext(key: string, context: Context | null): void;
    /**
     * @inheritDoc
     */
    setMeasurement(name: string, value: number, unit?: MeasurementUnit): void;
    /**
     * @inheritDoc
     */
    setMetadata(newMetadata: Partial<TransactionMetadata>): void;
    /**
     * @inheritDoc
     */
    finish(endTimestamp?: number): string | undefined;
    /**
     * @inheritDoc
     */
    toContext(): TransactionContext;
    /**
     * @inheritDoc
     */
    updateWithContext(transactionContext: TransactionContext): this;
    /**
     * @inheritdoc
     *
     * @experimental
     */
    getDynamicSamplingContext(): Readonly<Partial<DynamicSamplingContext>>;
}
//# sourceMappingURL=transaction.d.ts.map