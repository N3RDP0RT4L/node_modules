import { Hub } from '@sentry/types';
/**
 * Check if Sentry auto-instrumentation should be disabled.
 *
 * @param getCurrentHub A method to fetch the current hub
 * @returns boolean
 */
export declare function shouldDisableAutoInstrumentation(getCurrentHub: () => Hub): boolean;
//# sourceMappingURL=node-utils.d.ts.map