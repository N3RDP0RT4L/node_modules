import { Hub } from '@sentry/core';
import { EventProcessor, Integration } from '@sentry/types';
/** Tracing integration for Apollo */
export declare class Apollo implements Integration {
    /**
     * @inheritDoc
     */
    static id: string;
    /**
     * @inheritDoc
     */
    name: string;
    /**
     * @inheritDoc
     */
    setupOnce(_: (callback: EventProcessor) => void, getCurrentHub: () => Hub): void;
}
//# sourceMappingURL=apollo.d.ts.map