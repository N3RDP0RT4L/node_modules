import { Hub } from '@sentry/core';
import { EventProcessor, Integration } from '@sentry/types';
/** Tracing integration for graphql package */
export declare class GraphQL implements Integration {
    /**
     * @inheritDoc
     */
    static id: string;
    /**
     * @inheritDoc
     */
    name: string;
    /**
     * @inheritDoc
     */
    setupOnce(_: (callback: EventProcessor) => void, getCurrentHub: () => Hub): void;
}
//# sourceMappingURL=graphql.d.ts.map