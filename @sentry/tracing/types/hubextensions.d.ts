import { Hub } from '@sentry/core';
import { CustomSamplingContext, TransactionContext } from '@sentry/types';
import { IdleTransaction } from './idletransaction';
/**
 * Create new idle transaction.
 */
export declare function startIdleTransaction(hub: Hub, transactionContext: TransactionContext, idleTimeout: number, finalTimeout: number, onScope?: boolean, customSamplingContext?: CustomSamplingContext, heartbeatInterval?: number): IdleTransaction;
/**
 * @private
 */
export declare function _addTracingExtensions(): void;
/**
 * This patches the global object and injects the Tracing extensions methods
 */
export declare function addExtensionMethods(): void;
//# sourceMappingURL=hubextensions.d.ts.map