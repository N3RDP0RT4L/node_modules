import { FirstInputPolyfillEntry, NavigationTimingPolyfillEntry, PerformancePaintTiming } from '../types';
export interface PerformanceEntryHandler {
    (entry: PerformanceEntry): void;
}
interface PerformanceEntryMap {
    event: PerformanceEventTiming[];
    paint: PerformancePaintTiming[];
    'layout-shift': LayoutShift[];
    'largest-contentful-paint': LargestContentfulPaint[];
    'first-input': PerformanceEventTiming[] | FirstInputPolyfillEntry[];
    navigation: PerformanceNavigationTiming[] | NavigationTimingPolyfillEntry[];
    resource: PerformanceResourceTiming[];
    longtask: PerformanceEntry[];
}
/**
 * Takes a performance entry type and a callback function, and creates a
 * `PerformanceObserver` instance that will observe the specified entry type
 * with buffering enabled and call the callback _for each entry_.
 *
 * This function also feature-detects entry support and wraps the logic in a
 * try/catch to avoid errors in unsupporting browsers.
 */
export declare const observe: <K extends "navigation" | "resource" | "paint" | "event" | "layout-shift" | "largest-contentful-paint" | "first-input" | "longtask">(type: K, callback: (entries: PerformanceEntryMap[K]) => void, opts?: PerformanceObserverInit | undefined) => PerformanceObserver | undefined;
export {};
//# sourceMappingURL=observe.d.ts.map