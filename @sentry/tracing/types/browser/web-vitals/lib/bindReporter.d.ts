import { Metric, ReportCallback } from '../types';
export declare const bindReporter: (callback: ReportCallback, metric: Metric, reportAllChanges?: boolean | undefined) => (forceReport?: boolean | undefined) => void;
//# sourceMappingURL=bindReporter.d.ts.map