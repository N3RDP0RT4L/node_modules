import { Metric } from '../types';
export declare const initMetric: (name: "CLS" | "FCP" | "FID" | "INP" | "LCP" | "TTFB", value?: number | undefined) => Metric;
//# sourceMappingURL=initMetric.d.ts.map