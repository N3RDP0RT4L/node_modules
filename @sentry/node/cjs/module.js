Object.defineProperty(exports, '__esModule', { value: true });

const utils = require('@sentry/utils');

/** normalizes Windows paths */
function normalizePath(path) {
  return path
    .replace(/^[A-Z]:/, '') // remove Windows-style prefix
    .replace(/\\/g, '/'); // replace all `\` instances with `/`
}

/** Gets the module from a filename */
function getModule(filename) {
  if (!filename) {
    return;
  }

  const normalizedFilename = normalizePath(filename);

  // We could use optional chaining here but webpack does like that mixed with require
  const base = normalizePath(
    `${(require && require.main && require.main.filename && utils.dirname(require.main.filename)) || global.process.cwd()}/`,
  );

  // It's specifically a module
  const file = utils.basename(normalizedFilename, '.js');

  const path = utils.dirname(normalizedFilename);
  let n = path.lastIndexOf('/node_modules/');
  if (n > -1) {
    // /node_modules/ is 14 chars
    return `${path.substr(n + 14).replace(/\//g, '.')}:${file}`;
  }
  // Let's see if it's a part of the main module
  // To be a part of main module, it has to share the same base
  n = `${path}/`.lastIndexOf(base, 0);

  if (n === 0) {
    let moduleName = path.substr(base.length).replace(/\//g, '.');
    if (moduleName) {
      moduleName += ':';
    }
    moduleName += file;
    return moduleName;
  }
  return file;
}

exports.getModule = getModule;
//# sourceMappingURL=module.js.map
