Object.defineProperty(exports, '__esModule', { value: true });

const core = require('@sentry/core');
const client = require('./client.js');
require('./transports/index.js');
const sdk = require('./sdk.js');
const requestdata = require('./requestdata.js');
const utils = require('./utils.js');
const domain = require('domain');
const handlers = require('./handlers.js');
const index = require('./integrations/index.js');
const http = require('./transports/http.js');

;
;

;
;

const INTEGRATIONS = {
  ...core.Integrations,
  ...index,
};

// We need to patch domain on the global __SENTRY__ object to make it work for node in cross-platform packages like
// @sentry/core. If we don't do this, browser bundlers will have troubles resolving `require('domain')`.
const carrier = core.getMainCarrier();
if (carrier.__SENTRY__) {
  carrier.__SENTRY__.extensions = carrier.__SENTRY__.extensions || {};
  carrier.__SENTRY__.extensions.domain = carrier.__SENTRY__.extensions.domain || domain;
}

exports.Hub = core.Hub;
exports.SDK_VERSION = core.SDK_VERSION;
exports.Scope = core.Scope;
exports.addBreadcrumb = core.addBreadcrumb;
exports.addGlobalEventProcessor = core.addGlobalEventProcessor;
exports.captureEvent = core.captureEvent;
exports.captureException = core.captureException;
exports.captureMessage = core.captureMessage;
exports.configureScope = core.configureScope;
exports.createTransport = core.createTransport;
exports.getCurrentHub = core.getCurrentHub;
exports.getHubFromCarrier = core.getHubFromCarrier;
exports.makeMain = core.makeMain;
exports.setContext = core.setContext;
exports.setExtra = core.setExtra;
exports.setExtras = core.setExtras;
exports.setTag = core.setTag;
exports.setTags = core.setTags;
exports.setUser = core.setUser;
exports.startTransaction = core.startTransaction;
exports.withScope = core.withScope;
exports.NodeClient = client.NodeClient;
exports.close = sdk.close;
exports.defaultIntegrations = sdk.defaultIntegrations;
exports.defaultStackParser = sdk.defaultStackParser;
exports.flush = sdk.flush;
exports.getSentryRelease = sdk.getSentryRelease;
exports.init = sdk.init;
exports.lastEventId = sdk.lastEventId;
exports.DEFAULT_USER_INCLUDES = requestdata.DEFAULT_USER_INCLUDES;
exports.addRequestDataToEvent = requestdata.addRequestDataToEvent;
exports.extractRequestData = requestdata.extractRequestData;
exports.deepReadDirSync = utils.deepReadDirSync;
exports.Handlers = handlers;
exports.makeNodeTransport = http.makeNodeTransport;
exports.Integrations = INTEGRATIONS;
//# sourceMappingURL=index.js.map
