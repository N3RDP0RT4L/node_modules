var {
  _optionalChain
} = require('@sentry/utils/cjs/buildPolyfills');

Object.defineProperty(exports, '__esModule', { value: true });

const core = require('@sentry/core');
const utils = require('@sentry/utils');
const http = require('./utils/http.js');

const NODE_VERSION = utils.parseSemver(process.versions.node);

/**
 * The http module integration instruments Node's internal http module. It creates breadcrumbs, transactions for outgoing
 * http requests and attaches trace data when tracing is enabled via its `tracing` option.
 */
class Http  {
  /**
   * @inheritDoc
   */
   static __initStatic() {this.id = 'Http';}

  /**
   * @inheritDoc
   */
   __init() {this.name = Http.id;}

  /**
   * @inheritDoc
   */
   constructor(options = {}) {;Http.prototype.__init.call(this);
    this._breadcrumbs = typeof options.breadcrumbs === 'undefined' ? true : options.breadcrumbs;
    this._tracing = !options.tracing ? undefined : options.tracing === true ? {} : options.tracing;
  }

  /**
   * @inheritDoc
   */
   setupOnce(
    _addGlobalEventProcessor,
    setupOnceGetCurrentHub,
  ) {
    // No need to instrument if we don't want to track anything
    if (!this._breadcrumbs && !this._tracing) {
      return;
    }

    const clientOptions = _optionalChain([setupOnceGetCurrentHub, 'call', _ => _(), 'access', _2 => _2.getClient, 'call', _3 => _3(), 'optionalAccess', _4 => _4.getOptions, 'call', _5 => _5()]);

    // Do not auto-instrument for other instrumenter
    if (clientOptions && clientOptions.instrumenter !== 'sentry') {
      (typeof __SENTRY_DEBUG__ === 'undefined' || __SENTRY_DEBUG__) && utils.logger.log('HTTP Integration is skipped because of instrumenter configuration.');
      return;
    }

    // TODO (v8): `tracePropagationTargets` and `shouldCreateSpanForRequest` will be removed from clientOptions
    // and we will no longer have to do this optional merge, we can just pass `this._tracing` directly.
    const tracingOptions = this._tracing ? { ...clientOptions, ...this._tracing } : undefined;

    const wrappedHandlerMaker = _createWrappedRequestMethodFactory(this._breadcrumbs, tracingOptions);

    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const httpModule = require('http');
    utils.fill(httpModule, 'get', wrappedHandlerMaker);
    utils.fill(httpModule, 'request', wrappedHandlerMaker);

    // NOTE: Prior to Node 9, `https` used internals of `http` module, thus we don't patch it.
    // If we do, we'd get double breadcrumbs and double spans for `https` calls.
    // It has been changed in Node 9, so for all versions equal and above, we patch `https` separately.
    if (NODE_VERSION.major && NODE_VERSION.major > 8) {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const httpsModule = require('https');
      utils.fill(httpsModule, 'get', wrappedHandlerMaker);
      utils.fill(httpsModule, 'request', wrappedHandlerMaker);
    }
  }
}Http.__initStatic();

// for ease of reading below

/**
 * Function which creates a function which creates wrapped versions of internal `request` and `get` calls within `http`
 * and `https` modules. (NB: Not a typo - this is a creator^2!)
 *
 * @param breadcrumbsEnabled Whether or not to record outgoing requests as breadcrumbs
 * @param tracingEnabled Whether or not to record outgoing requests as tracing spans
 *
 * @returns A function which accepts the exiting handler and returns a wrapped handler
 */
function _createWrappedRequestMethodFactory(
  breadcrumbsEnabled,
  tracingOptions,
) {
  // We're caching results so we don't have to recompute regexp every time we create a request.
  const createSpanUrlMap = {};
  const headersUrlMap = {};

  const shouldCreateSpan = (url) => {
    if (_optionalChain([tracingOptions, 'optionalAccess', _6 => _6.shouldCreateSpanForRequest]) === undefined) {
      return true;
    }

    if (createSpanUrlMap[url]) {
      return createSpanUrlMap[url];
    }

    createSpanUrlMap[url] = tracingOptions.shouldCreateSpanForRequest(url);

    return createSpanUrlMap[url];
  };

  const shouldAttachTraceData = (url) => {
    if (_optionalChain([tracingOptions, 'optionalAccess', _7 => _7.tracePropagationTargets]) === undefined) {
      return true;
    }

    if (headersUrlMap[url]) {
      return headersUrlMap[url];
    }

    headersUrlMap[url] = utils.stringMatchesSomePattern(url, tracingOptions.tracePropagationTargets);

    return headersUrlMap[url];
  };

  return function wrappedRequestMethodFactory(originalRequestMethod) {
    return function wrappedMethod( ...args) {
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      const httpModule = this;

      const requestArgs = http.normalizeRequestArgs(this, args);
      const requestOptions = requestArgs[0];
      const requestUrl = http.extractUrl(requestOptions);

      // we don't want to record requests to Sentry as either breadcrumbs or spans, so just use the original method
      if (http.isSentryRequest(requestUrl)) {
        return originalRequestMethod.apply(httpModule, requestArgs);
      }

      let requestSpan;
      let parentSpan;

      const scope = core.getCurrentHub().getScope();

      if (scope && tracingOptions && shouldCreateSpan(requestUrl)) {
        parentSpan = scope.getSpan();

        if (parentSpan) {
          requestSpan = parentSpan.startChild({
            description: `${requestOptions.method || 'GET'} ${requestUrl}`,
            op: 'http.client',
          });

          if (shouldAttachTraceData(requestUrl)) {
            const sentryTraceHeader = requestSpan.toTraceparent();
            (typeof __SENTRY_DEBUG__ === 'undefined' || __SENTRY_DEBUG__) &&
              utils.logger.log(
                `[Tracing] Adding sentry-trace header ${sentryTraceHeader} to outgoing request to "${requestUrl}": `,
              );

            requestOptions.headers = {
              ...requestOptions.headers,
              'sentry-trace': sentryTraceHeader,
            };

            if (parentSpan.transaction) {
              const dynamicSamplingContext = parentSpan.transaction.getDynamicSamplingContext();
              const sentryBaggageHeader = utils.dynamicSamplingContextToSentryBaggageHeader(dynamicSamplingContext);

              let newBaggageHeaderField;
              if (!requestOptions.headers || !requestOptions.headers.baggage) {
                newBaggageHeaderField = sentryBaggageHeader;
              } else if (!sentryBaggageHeader) {
                newBaggageHeaderField = requestOptions.headers.baggage;
              } else if (Array.isArray(requestOptions.headers.baggage)) {
                newBaggageHeaderField = [...requestOptions.headers.baggage, sentryBaggageHeader];
              } else {
                // Type-cast explanation:
                // Technically this the following could be of type `(number | string)[]` but for the sake of simplicity
                // we say this is undefined behaviour, since it would not be baggage spec conform if the user did this.
                newBaggageHeaderField = [requestOptions.headers.baggage, sentryBaggageHeader] ;
              }

              requestOptions.headers = {
                ...requestOptions.headers,
                // Setting a hader to `undefined` will crash in node so we only set the baggage header when it's defined
                ...(newBaggageHeaderField && { baggage: newBaggageHeaderField }),
              };
            }
          } else {
            (typeof __SENTRY_DEBUG__ === 'undefined' || __SENTRY_DEBUG__) &&
              utils.logger.log(
                `[Tracing] Not adding sentry-trace header to outgoing request (${requestUrl}) due to mismatching tracePropagationTargets option.`,
              );
          }

          const transaction = parentSpan.transaction;
          if (transaction) {
            transaction.metadata.propagations++;
          }
        }
      }

      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      return originalRequestMethod
        .apply(httpModule, requestArgs)
        .once('response', function ( res) {
          // eslint-disable-next-line @typescript-eslint/no-this-alias
          const req = this;
          if (breadcrumbsEnabled) {
            addRequestBreadcrumb('response', requestUrl, req, res);
          }
          if (requestSpan) {
            if (res.statusCode) {
              requestSpan.setHttpStatus(res.statusCode);
            }
            requestSpan.description = http.cleanSpanDescription(requestSpan.description, requestOptions, req);
            requestSpan.finish();
          }
        })
        .once('error', function () {
          // eslint-disable-next-line @typescript-eslint/no-this-alias
          const req = this;

          if (breadcrumbsEnabled) {
            addRequestBreadcrumb('error', requestUrl, req);
          }
          if (requestSpan) {
            requestSpan.setHttpStatus(500);
            requestSpan.description = http.cleanSpanDescription(requestSpan.description, requestOptions, req);
            requestSpan.finish();
          }
        });
    };
  };
}

/**
 * Captures Breadcrumb based on provided request/response pair
 */
function addRequestBreadcrumb(event, url, req, res) {
  if (!core.getCurrentHub().getIntegration(Http)) {
    return;
  }

  core.getCurrentHub().addBreadcrumb(
    {
      category: 'http',
      data: {
        method: req.method,
        status_code: res && res.statusCode,
        url,
      },
      type: 'http',
    },
    {
      event,
      request: req,
      response: res,
    },
  );
}

exports.Http = Http;
//# sourceMappingURL=http.js.map
