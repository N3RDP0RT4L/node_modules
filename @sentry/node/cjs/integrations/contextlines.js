var {
  _optionalChain
} = require('@sentry/utils/cjs/buildPolyfills');

Object.defineProperty(exports, '__esModule', { value: true });

const utils = require('@sentry/utils');
const fs = require('fs');
const lru_map = require('lru_map');

const FILE_CONTENT_CACHE = new lru_map.LRUMap(100);
const DEFAULT_LINES_OF_CONTEXT = 7;

// TODO: Replace with promisify when minimum supported node >= v8
function readTextFileAsync(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
}

/**
 * Resets the file cache. Exists for testing purposes.
 * @hidden
 */
function resetFileContentCache() {
  FILE_CONTENT_CACHE.clear();
}

/** Add node modules / packages to the event */
class ContextLines  {
  /**
   * @inheritDoc
   */
   static __initStatic() {this.id = 'ContextLines';}

  /**
   * @inheritDoc
   */
   __init() {this.name = ContextLines.id;}

   constructor(  _options = {}) {;this._options = _options;ContextLines.prototype.__init.call(this);}

  /** Get's the number of context lines to add */
   get _contextLines() {
    return this._options.frameContextLines !== undefined ? this._options.frameContextLines : DEFAULT_LINES_OF_CONTEXT;
  }

  /**
   * @inheritDoc
   */
   setupOnce(addGlobalEventProcessor) {
    addGlobalEventProcessor(event => this.addSourceContext(event));
  }

  /** Processes an event and adds context lines */
   async addSourceContext(event) {
    if (this._contextLines > 0 && _optionalChain([event, 'access', _2 => _2.exception, 'optionalAccess', _3 => _3.values])) {
      for (const exception of event.exception.values) {
        if (_optionalChain([exception, 'access', _4 => _4.stacktrace, 'optionalAccess', _5 => _5.frames])) {
          await this.addSourceContextToFrames(exception.stacktrace.frames);
        }
      }
    }

    return event;
  }

  /** Adds context lines to frames */
   async addSourceContextToFrames(frames) {
    const contextLines = this._contextLines;

    for (const frame of frames) {
      // Only add context if we have a filename and it hasn't already been added
      if (frame.filename && frame.context_line === undefined) {
        const sourceFile = await _readSourceFile(frame.filename);

        if (sourceFile) {
          try {
            const lines = sourceFile.split('\n');
            utils.addContextToFrame(lines, frame, contextLines);
          } catch (e) {
            // anomaly, being defensive in case
            // unlikely to ever happen in practice but can definitely happen in theory
          }
        }
      }
    }
  }
}ContextLines.__initStatic();

/**
 * Reads file contents and caches them in a global LRU cache.
 *
 * @param filename filepath to read content from.
 */
async function _readSourceFile(filename) {
  const cachedFile = FILE_CONTENT_CACHE.get(filename);
  // We have a cache hit
  if (cachedFile !== undefined) {
    return cachedFile;
  }

  let content = null;
  try {
    content = await readTextFileAsync(filename);
  } catch (_) {
    //
  }

  FILE_CONTENT_CACHE.set(filename, content);
  return content;
}

exports.ContextLines = ContextLines;
exports.resetFileContentCache = resetFileContentCache;
//# sourceMappingURL=contextlines.js.map
