import { Integrations, getMainCarrier } from '@sentry/core';
export { Hub, SDK_VERSION, Scope, addBreadcrumb, addGlobalEventProcessor, captureEvent, captureException, captureMessage, configureScope, createTransport, getCurrentHub, getHubFromCarrier, makeMain, setContext, setExtra, setExtras, setTag, setTags, setUser, startTransaction, withScope } from '@sentry/core';
export { NodeClient } from './client.js';
import './transports/index.js';
export { close, defaultIntegrations, defaultStackParser, flush, getSentryRelease, init, lastEventId } from './sdk.js';
export { DEFAULT_USER_INCLUDES, addRequestDataToEvent, extractRequestData } from './requestdata.js';
export { deepReadDirSync } from './utils.js';
import * as domain from 'domain';
import * as handlers from './handlers.js';
export { handlers as Handlers };
import * as index from './integrations/index.js';
export { makeNodeTransport } from './transports/http.js';

;
;

;
;

const INTEGRATIONS = {
  ...Integrations,
  ...index,
};

// We need to patch domain on the global __SENTRY__ object to make it work for node in cross-platform packages like
// @sentry/core. If we don't do this, browser bundlers will have troubles resolving `require('domain')`.
const carrier = getMainCarrier();
if (carrier.__SENTRY__) {
  carrier.__SENTRY__.extensions = carrier.__SENTRY__.extensions || {};
  carrier.__SENTRY__.extensions.domain = carrier.__SENTRY__.extensions.domain || domain;
}

export { INTEGRATIONS as Integrations };
//# sourceMappingURL=index.js.map
