/// <reference types="node" />
import { AddRequestDataToEventOptions } from '@sentry/utils';
import * as http from 'http';
import type { ParseRequestOptions } from './requestDataDeprecated';
/**
 * Express-compatible tracing handler.
 * @see Exposed as `Handlers.tracingHandler`
 */
export declare function tracingHandler(): (req: http.IncomingMessage, res: http.ServerResponse, next: (error?: any) => void) => void;
export declare type RequestHandlerOptions = (ParseRequestOptions | AddRequestDataToEventOptions) & {
    flushTimeout?: number;
};
/**
 * Express compatible request handler.
 * @see Exposed as `Handlers.requestHandler`
 */
export declare function requestHandler(options?: RequestHandlerOptions): (req: http.IncomingMessage, res: http.ServerResponse, next: (error?: any) => void) => void;
/** JSDoc */
interface MiddlewareError extends Error {
    status?: number | string;
    statusCode?: number | string;
    status_code?: number | string;
    output?: {
        statusCode?: number | string;
    };
}
/**
 * Express compatible error handler.
 * @see Exposed as `Handlers.errorHandler`
 */
export declare function errorHandler(options?: {
    /**
     * Callback method deciding whether error should be captured and sent to Sentry
     * @param error Captured middleware error
     */
    shouldHandleError?(error: MiddlewareError): boolean;
}): (error: MiddlewareError, req: http.IncomingMessage, res: http.ServerResponse, next: (error: MiddlewareError) => void) => void;
export type { ParseRequestOptions, ExpressRequest } from './requestDataDeprecated';
export { parseRequest, extractRequestData } from './requestDataDeprecated';
//# sourceMappingURL=handlers.d.ts.map