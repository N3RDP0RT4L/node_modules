import { Event, ExtractedNodeRequestData, PolymorphicRequest, Transaction, TransactionSource } from '@sentry/types';
declare const DEFAULT_REQUEST_INCLUDES: string[];
export declare const DEFAULT_USER_INCLUDES: string[];
/**
 * Options deciding what parts of the request to use when enhancing an event
 */
export declare type AddRequestDataToEventOptions = {
    /** Flags controlling whether each type of data should be added to the event */
    include?: {
        ip?: boolean;
        request?: boolean | Array<typeof DEFAULT_REQUEST_INCLUDES[number]>;
        transaction?: boolean | TransactionNamingScheme;
        user?: boolean | Array<typeof DEFAULT_USER_INCLUDES[number]>;
    };
};
export declare type TransactionNamingScheme = 'path' | 'methodPath' | 'handler';
/**
 * Sets parameterized route as transaction name e.g.: `GET /users/:id`
 * Also adds more context data on the transaction from the request
 */
export declare function addRequestDataToTransaction(transaction: Transaction | undefined, req: PolymorphicRequest): void;
/**
 * Extracts a complete and parameterized path from the request object and uses it to construct transaction name.
 * If the parameterized transaction name cannot be extracted, we fall back to the raw URL.
 *
 * Additionally, this function determines and returns the transaction name source
 *
 * eg. GET /mountpoint/user/:id
 *
 * @param req A request object
 * @param options What to include in the transaction name (method, path, or a custom route name to be
 *                used instead of the request's route)
 *
 * @returns A tuple of the fully constructed transaction name [0] and its source [1] (can be either 'route' or 'url')
 */
export declare function extractPathForTransaction(req: PolymorphicRequest, options?: {
    path?: boolean;
    method?: boolean;
    customRoute?: string;
}): [string, TransactionSource];
/**
 * Normalize data from the request object
 *
 * @param req The request object from which to extract data
 * @param options.include An optional array of keys to include in the normalized data. Defaults to
 * DEFAULT_REQUEST_INCLUDES if not provided.
 * @param options.deps Injected, platform-specific dependencies
 *
 * @returns An object containing normalized request data
 */
export declare function extractRequestData(req: PolymorphicRequest, options?: {
    include?: string[];
}): ExtractedNodeRequestData;
/**
 * Add data from the given request to the given event
 *
 * @param event The event to which the request data will be added
 * @param req Request object
 * @param options.include Flags to control what data is included
 *
 * @returns The mutated `Event` object
 */
export declare function addRequestDataToEvent(event: Event, req: PolymorphicRequest, options?: AddRequestDataToEventOptions): Event;
export {};
//# sourceMappingURL=requestdata.d.ts.map