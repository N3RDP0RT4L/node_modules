export type { Breadcrumb, BreadcrumbHint, PolymorphicRequest, Request, SdkInfo, Event, EventHint, Exception, Session, Severity, SeverityLevel, StackFrame, Stacktrace, Thread, User, Span, } from '@sentry/types';
export type { AddRequestDataToEventOptions } from '@sentry/utils';
export type { TransactionNamingScheme } from './requestdata';
export type { NodeOptions } from './types';
export { addGlobalEventProcessor, addBreadcrumb, captureException, captureEvent, captureMessage, configureScope, createTransport, getHubFromCarrier, getCurrentHub, Hub, makeMain, Scope, startTransaction, SDK_VERSION, setContext, setExtra, setExtras, setTag, setTags, setUser, withScope, } from '@sentry/core';
export { NodeClient } from './client';
export { makeNodeTransport } from './transports';
export { defaultIntegrations, init, defaultStackParser, lastEventId, flush, close, getSentryRelease } from './sdk';
export { addRequestDataToEvent, DEFAULT_USER_INCLUDES, extractRequestData } from './requestdata';
export { deepReadDirSync } from './utils';
import { Integrations as CoreIntegrations } from '@sentry/core';
import * as Handlers from './handlers';
import * as NodeIntegrations from './integrations';
declare const INTEGRATIONS: {
    Console: typeof NodeIntegrations.Console;
    Http: typeof NodeIntegrations.Http;
    OnUncaughtException: typeof NodeIntegrations.OnUncaughtException;
    OnUnhandledRejection: typeof NodeIntegrations.OnUnhandledRejection;
    LinkedErrors: typeof NodeIntegrations.LinkedErrors;
    Modules: typeof NodeIntegrations.Modules;
    ContextLines: typeof NodeIntegrations.ContextLines;
    Context: typeof NodeIntegrations.Context;
    RequestData: typeof NodeIntegrations.RequestData;
    FunctionToString: typeof CoreIntegrations.FunctionToString;
    InboundFilters: typeof CoreIntegrations.InboundFilters;
};
export { INTEGRATIONS as Integrations, Handlers };
//# sourceMappingURL=index.d.ts.map