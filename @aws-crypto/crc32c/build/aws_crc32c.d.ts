import { Hash, SourceData } from "@aws-sdk/types";
export declare class AwsCrc32c implements Hash {
    private readonly crc32c;
    update(toHash: SourceData): void;
    digest(): Promise<Uint8Array>;
}
