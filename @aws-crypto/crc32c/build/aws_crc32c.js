"use strict";
// Copyright Amazon.com Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0
Object.defineProperty(exports, "__esModule", { value: true });
exports.AwsCrc32c = void 0;
var tslib_1 = require("tslib");
var util_1 = require("@aws-crypto/util");
var index_1 = require("./index");
var AwsCrc32c = /** @class */ (function () {
    function AwsCrc32c() {
        this.crc32c = new index_1.Crc32c();
    }
    AwsCrc32c.prototype.update = function (toHash) {
        if ((0, util_1.isEmptyData)(toHash))
            return;
        this.crc32c.update((0, util_1.convertToBuffer)(toHash));
    };
    AwsCrc32c.prototype.digest = function () {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function () {
            return (0, tslib_1.__generator)(this, function (_a) {
                return [2 /*return*/, (0, util_1.numToUint8)(this.crc32c.digest())];
            });
        });
    };
    return AwsCrc32c;
}());
exports.AwsCrc32c = AwsCrc32c;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXdzX2NyYzMyYy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9hd3NfY3JjMzJjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxvRUFBb0U7QUFDcEUsc0NBQXNDOzs7O0FBR3RDLHlDQUE0RTtBQUM1RSxpQ0FBaUM7QUFFakM7SUFBQTtRQUNtQixXQUFNLEdBQUcsSUFBSSxjQUFNLEVBQUUsQ0FBQztJQVd6QyxDQUFDO0lBVEMsMEJBQU0sR0FBTixVQUFPLE1BQWtCO1FBQ3ZCLElBQUksSUFBQSxrQkFBVyxFQUFDLE1BQU0sQ0FBQztZQUFFLE9BQU87UUFFaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBQSxzQkFBZSxFQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVLLDBCQUFNLEdBQVo7OztnQkFDRSxzQkFBTyxJQUFBLGlCQUFVLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFDOzs7S0FDekM7SUFDSCxnQkFBQztBQUFELENBQUMsQUFaRCxJQVlDO0FBWlksOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDb3B5cmlnaHQgQW1hem9uLmNvbSBJbmMuIG9yIGl0cyBhZmZpbGlhdGVzLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuLy8gU1BEWC1MaWNlbnNlLUlkZW50aWZpZXI6IEFwYWNoZS0yLjBcblxuaW1wb3J0IHsgSGFzaCwgU291cmNlRGF0YSB9IGZyb20gXCJAYXdzLXNkay90eXBlc1wiO1xuaW1wb3J0IHsgY29udmVydFRvQnVmZmVyLCBpc0VtcHR5RGF0YSwgbnVtVG9VaW50OCB9IGZyb20gXCJAYXdzLWNyeXB0by91dGlsXCI7XG5pbXBvcnQgeyBDcmMzMmMgfSBmcm9tIFwiLi9pbmRleFwiO1xuXG5leHBvcnQgY2xhc3MgQXdzQ3JjMzJjIGltcGxlbWVudHMgSGFzaCB7XG4gIHByaXZhdGUgcmVhZG9ubHkgY3JjMzJjID0gbmV3IENyYzMyYygpO1xuXG4gIHVwZGF0ZSh0b0hhc2g6IFNvdXJjZURhdGEpIHtcbiAgICBpZiAoaXNFbXB0eURhdGEodG9IYXNoKSkgcmV0dXJuO1xuXG4gICAgdGhpcy5jcmMzMmMudXBkYXRlKGNvbnZlcnRUb0J1ZmZlcih0b0hhc2gpKTtcbiAgfVxuXG4gIGFzeW5jIGRpZ2VzdCgpOiBQcm9taXNlPFVpbnQ4QXJyYXk+IHtcbiAgICByZXR1cm4gbnVtVG9VaW50OCh0aGlzLmNyYzMyYy5kaWdlc3QoKSk7XG4gIH1cbn1cbiJdfQ==