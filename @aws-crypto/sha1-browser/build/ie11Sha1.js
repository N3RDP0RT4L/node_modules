"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sha1 = void 0;
var isEmptyData_1 = require("./isEmptyData");
var constants_1 = require("./constants");
var util_utf8_browser_1 = require("@aws-sdk/util-utf8-browser");
var util_locate_window_1 = require("@aws-sdk/util-locate-window");
var Sha1 = /** @class */ (function () {
    function Sha1(secret) {
        if (secret) {
            this.operation = getKeyPromise(secret).then(function (keyData) {
                return (0, util_locate_window_1.locateWindow)().msCrypto.subtle.sign(constants_1.SHA_1_HMAC_ALGO, keyData);
            });
            this.operation.catch(function () { });
        }
        else {
            this.operation = Promise.resolve((0, util_locate_window_1.locateWindow)().msCrypto.subtle.digest("SHA-1"));
        }
    }
    Sha1.prototype.update = function (toHash) {
        var _this = this;
        if ((0, isEmptyData_1.isEmptyData)(toHash)) {
            return;
        }
        this.operation = this.operation.then(function (operation) {
            operation.onerror = function () {
                _this.operation = Promise.reject(new Error("Error encountered updating hash"));
            };
            operation.process(toArrayBufferView(toHash));
            return operation;
        });
        this.operation.catch(function () { });
    };
    Sha1.prototype.digest = function () {
        return this.operation.then(function (operation) {
            return new Promise(function (resolve, reject) {
                operation.onerror = function () {
                    reject(new Error("Error encountered finalizing hash"));
                };
                operation.oncomplete = function () {
                    if (operation.result) {
                        resolve(new Uint8Array(operation.result));
                    }
                    reject(new Error("Error encountered finalizing hash"));
                };
                operation.finish();
            });
        });
    };
    return Sha1;
}());
exports.Sha1 = Sha1;
function getKeyPromise(secret) {
    return new Promise(function (resolve, reject) {
        var keyOperation = (0, util_locate_window_1.locateWindow)().msCrypto.subtle.importKey("raw", toArrayBufferView(secret), constants_1.SHA_1_HMAC_ALGO, false, ["sign"]);
        keyOperation.oncomplete = function () {
            if (keyOperation.result) {
                resolve(keyOperation.result);
            }
            reject(new Error("ImportKey completed without importing key."));
        };
        keyOperation.onerror = function () {
            reject(new Error("ImportKey failed to import key."));
        };
    });
}
function toArrayBufferView(data) {
    if (typeof data === "string") {
        return (0, util_utf8_browser_1.fromUtf8)(data);
    }
    if (ArrayBuffer.isView(data)) {
        return new Uint8Array(data.buffer, data.byteOffset, data.byteLength / Uint8Array.BYTES_PER_ELEMENT);
    }
    return new Uint8Array(data);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWUxMVNoYTEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaWUxMVNoYTEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsNkNBQTRDO0FBQzVDLHlDQUE4QztBQUU5QyxnRUFBc0Q7QUFFdEQsa0VBQTJEO0FBRTNEO0lBR0UsY0FBWSxNQUFtQjtRQUM3QixJQUFJLE1BQU0sRUFBRTtZQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQU87Z0JBQ2xELE9BQUMsSUFBQSxpQ0FBWSxHQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQy9DLDJCQUFlLEVBQ2YsT0FBTyxDQUNSO1lBSEQsQ0FHQyxDQUNGLENBQUM7WUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxjQUFPLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQzdCLElBQUEsaUNBQVksR0FBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUM3RCxDQUFDO1NBQ0g7SUFDSCxDQUFDO0lBRUQscUJBQU0sR0FBTixVQUFPLE1BQWtCO1FBQXpCLGlCQWdCQztRQWZDLElBQUksSUFBQSx5QkFBVyxFQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3ZCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBQyxTQUFTO1lBQzdDLFNBQVMsQ0FBQyxPQUFPLEdBQUc7Z0JBQ2xCLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FDN0IsSUFBSSxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FDN0MsQ0FBQztZQUNKLENBQUMsQ0FBQztZQUNGLFNBQVMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUU3QyxPQUFPLFNBQVMsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLGNBQU8sQ0FBQyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELHFCQUFNLEdBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUN4QixVQUFDLFNBQVM7WUFDUixPQUFBLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07Z0JBQzFCLFNBQVMsQ0FBQyxPQUFPLEdBQUc7b0JBQ2xCLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pELENBQUMsQ0FBQztnQkFDRixTQUFTLENBQUMsVUFBVSxHQUFHO29CQUNyQixJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7d0JBQ3BCLE9BQU8sQ0FBQyxJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztxQkFDM0M7b0JBQ0QsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztnQkFDekQsQ0FBQyxDQUFDO2dCQUVGLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUM7UUFaRixDQVlFLENBQ0wsQ0FBQztJQUNKLENBQUM7SUFDSCxXQUFDO0FBQUQsQ0FBQyxBQXZERCxJQXVEQztBQXZEWSxvQkFBSTtBQXlEakIsU0FBUyxhQUFhLENBQUMsTUFBa0I7SUFDdkMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1FBQ2pDLElBQU0sWUFBWSxHQUFJLElBQUEsaUNBQVksR0FBZSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUN6RSxLQUFLLEVBQ0wsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQ3pCLDJCQUFlLEVBQ2YsS0FBSyxFQUNMLENBQUMsTUFBTSxDQUFDLENBQ1QsQ0FBQztRQUVGLFlBQVksQ0FBQyxVQUFVLEdBQUc7WUFDeEIsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO2dCQUN2QixPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlCO1lBRUQsTUFBTSxDQUFDLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUMsQ0FBQztRQUNsRSxDQUFDLENBQUM7UUFDRixZQUFZLENBQUMsT0FBTyxHQUFHO1lBQ3JCLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBRUQsU0FBUyxpQkFBaUIsQ0FBQyxJQUFnQjtJQUN6QyxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtRQUM1QixPQUFPLElBQUEsNEJBQVEsRUFBQyxJQUFJLENBQUMsQ0FBQztLQUN2QjtJQUVELElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtRQUM1QixPQUFPLElBQUksVUFBVSxDQUNuQixJQUFJLENBQUMsTUFBTSxFQUNYLElBQUksQ0FBQyxVQUFVLEVBQ2YsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsaUJBQWlCLENBQy9DLENBQUM7S0FDSDtJQUVELE9BQU8sSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDOUIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGlzRW1wdHlEYXRhIH0gZnJvbSBcIi4vaXNFbXB0eURhdGFcIjtcbmltcG9ydCB7IFNIQV8xX0hNQUNfQUxHTyB9IGZyb20gXCIuL2NvbnN0YW50c1wiO1xuaW1wb3J0IHsgSGFzaCwgU291cmNlRGF0YSB9IGZyb20gXCJAYXdzLXNkay90eXBlc1wiO1xuaW1wb3J0IHsgZnJvbVV0ZjggfSBmcm9tIFwiQGF3cy1zZGsvdXRpbC11dGY4LWJyb3dzZXJcIjtcbmltcG9ydCB7IENyeXB0b09wZXJhdGlvbiwgS2V5LCBNc1dpbmRvdyB9IGZyb20gXCJAYXdzLWNyeXB0by9pZTExLWRldGVjdGlvblwiO1xuaW1wb3J0IHsgbG9jYXRlV2luZG93IH0gZnJvbSBcIkBhd3Mtc2RrL3V0aWwtbG9jYXRlLXdpbmRvd1wiO1xuXG5leHBvcnQgY2xhc3MgU2hhMSBpbXBsZW1lbnRzIEhhc2gge1xuICBwcml2YXRlIG9wZXJhdGlvbjogUHJvbWlzZTxDcnlwdG9PcGVyYXRpb24+O1xuXG4gIGNvbnN0cnVjdG9yKHNlY3JldD86IFNvdXJjZURhdGEpIHtcbiAgICBpZiAoc2VjcmV0KSB7XG4gICAgICB0aGlzLm9wZXJhdGlvbiA9IGdldEtleVByb21pc2Uoc2VjcmV0KS50aGVuKChrZXlEYXRhKSA9PlxuICAgICAgICAobG9jYXRlV2luZG93KCkgYXMgTXNXaW5kb3cpLm1zQ3J5cHRvLnN1YnRsZS5zaWduKFxuICAgICAgICAgIFNIQV8xX0hNQUNfQUxHTyxcbiAgICAgICAgICBrZXlEYXRhXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgICB0aGlzLm9wZXJhdGlvbi5jYXRjaCgoKSA9PiB7fSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMub3BlcmF0aW9uID0gUHJvbWlzZS5yZXNvbHZlKFxuICAgICAgICAobG9jYXRlV2luZG93KCkgYXMgTXNXaW5kb3cpLm1zQ3J5cHRvLnN1YnRsZS5kaWdlc3QoXCJTSEEtMVwiKVxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGUodG9IYXNoOiBTb3VyY2VEYXRhKTogdm9pZCB7XG4gICAgaWYgKGlzRW1wdHlEYXRhKHRvSGFzaCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLm9wZXJhdGlvbiA9IHRoaXMub3BlcmF0aW9uLnRoZW4oKG9wZXJhdGlvbikgPT4ge1xuICAgICAgb3BlcmF0aW9uLm9uZXJyb3IgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMub3BlcmF0aW9uID0gUHJvbWlzZS5yZWplY3QoXG4gICAgICAgICAgbmV3IEVycm9yKFwiRXJyb3IgZW5jb3VudGVyZWQgdXBkYXRpbmcgaGFzaFwiKVxuICAgICAgICApO1xuICAgICAgfTtcbiAgICAgIG9wZXJhdGlvbi5wcm9jZXNzKHRvQXJyYXlCdWZmZXJWaWV3KHRvSGFzaCkpO1xuXG4gICAgICByZXR1cm4gb3BlcmF0aW9uO1xuICAgIH0pO1xuICAgIHRoaXMub3BlcmF0aW9uLmNhdGNoKCgpID0+IHt9KTtcbiAgfVxuXG4gIGRpZ2VzdCgpOiBQcm9taXNlPFVpbnQ4QXJyYXk+IHtcbiAgICByZXR1cm4gdGhpcy5vcGVyYXRpb24udGhlbjxVaW50OEFycmF5PihcbiAgICAgIChvcGVyYXRpb24pID0+XG4gICAgICAgIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICBvcGVyYXRpb24ub25lcnJvciA9ICgpID0+IHtcbiAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoXCJFcnJvciBlbmNvdW50ZXJlZCBmaW5hbGl6aW5nIGhhc2hcIikpO1xuICAgICAgICAgIH07XG4gICAgICAgICAgb3BlcmF0aW9uLm9uY29tcGxldGUgPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAob3BlcmF0aW9uLnJlc3VsdCkge1xuICAgICAgICAgICAgICByZXNvbHZlKG5ldyBVaW50OEFycmF5KG9wZXJhdGlvbi5yZXN1bHQpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJlamVjdChuZXcgRXJyb3IoXCJFcnJvciBlbmNvdW50ZXJlZCBmaW5hbGl6aW5nIGhhc2hcIikpO1xuICAgICAgICAgIH07XG5cbiAgICAgICAgICBvcGVyYXRpb24uZmluaXNoKCk7XG4gICAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRLZXlQcm9taXNlKHNlY3JldDogU291cmNlRGF0YSk6IFByb21pc2U8S2V5PiB7XG4gIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgY29uc3Qga2V5T3BlcmF0aW9uID0gKGxvY2F0ZVdpbmRvdygpIGFzIE1zV2luZG93KS5tc0NyeXB0by5zdWJ0bGUuaW1wb3J0S2V5KFxuICAgICAgXCJyYXdcIixcbiAgICAgIHRvQXJyYXlCdWZmZXJWaWV3KHNlY3JldCksXG4gICAgICBTSEFfMV9ITUFDX0FMR08sXG4gICAgICBmYWxzZSxcbiAgICAgIFtcInNpZ25cIl1cbiAgICApO1xuXG4gICAga2V5T3BlcmF0aW9uLm9uY29tcGxldGUgPSAoKSA9PiB7XG4gICAgICBpZiAoa2V5T3BlcmF0aW9uLnJlc3VsdCkge1xuICAgICAgICByZXNvbHZlKGtleU9wZXJhdGlvbi5yZXN1bHQpO1xuICAgICAgfVxuXG4gICAgICByZWplY3QobmV3IEVycm9yKFwiSW1wb3J0S2V5IGNvbXBsZXRlZCB3aXRob3V0IGltcG9ydGluZyBrZXkuXCIpKTtcbiAgICB9O1xuICAgIGtleU9wZXJhdGlvbi5vbmVycm9yID0gKCkgPT4ge1xuICAgICAgcmVqZWN0KG5ldyBFcnJvcihcIkltcG9ydEtleSBmYWlsZWQgdG8gaW1wb3J0IGtleS5cIikpO1xuICAgIH07XG4gIH0pO1xufVxuXG5mdW5jdGlvbiB0b0FycmF5QnVmZmVyVmlldyhkYXRhOiBTb3VyY2VEYXRhKTogVWludDhBcnJheSB7XG4gIGlmICh0eXBlb2YgZGF0YSA9PT0gXCJzdHJpbmdcIikge1xuICAgIHJldHVybiBmcm9tVXRmOChkYXRhKTtcbiAgfVxuXG4gIGlmIChBcnJheUJ1ZmZlci5pc1ZpZXcoZGF0YSkpIHtcbiAgICByZXR1cm4gbmV3IFVpbnQ4QXJyYXkoXG4gICAgICBkYXRhLmJ1ZmZlcixcbiAgICAgIGRhdGEuYnl0ZU9mZnNldCxcbiAgICAgIGRhdGEuYnl0ZUxlbmd0aCAvIFVpbnQ4QXJyYXkuQllURVNfUEVSX0VMRU1FTlRcbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIG5ldyBVaW50OEFycmF5KGRhdGEpO1xufVxuIl19