"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sha1 = void 0;
var ie11Sha1_1 = require("./ie11Sha1");
var webCryptoSha1_1 = require("./webCryptoSha1");
var supports_web_crypto_1 = require("@aws-crypto/supports-web-crypto");
var ie11_detection_1 = require("@aws-crypto/ie11-detection");
var util_locate_window_1 = require("@aws-sdk/util-locate-window");
var Sha1 = /** @class */ (function () {
    function Sha1(secret) {
        if ((0, supports_web_crypto_1.supportsWebCrypto)((0, util_locate_window_1.locateWindow)())) {
            this.hash = new webCryptoSha1_1.Sha1(secret);
        }
        else if ((0, ie11_detection_1.isMsWindow)((0, util_locate_window_1.locateWindow)())) {
            this.hash = new ie11Sha1_1.Sha1(secret);
        }
        else {
            throw new Error("SHA1 not supported");
        }
    }
    Sha1.prototype.update = function (data, encoding) {
        this.hash.update(data, encoding);
    };
    Sha1.prototype.digest = function () {
        return this.hash.digest();
    };
    return Sha1;
}());
exports.Sha1 = Sha1;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Jvc3NQbGF0Zm9ybVNoYTEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvY3Jvc3NQbGF0Zm9ybVNoYTEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsdUNBQThDO0FBQzlDLGlEQUF3RDtBQUV4RCx1RUFBb0U7QUFDcEUsNkRBQXdEO0FBQ3hELGtFQUEyRDtBQUUzRDtJQUdFLGNBQVksTUFBbUI7UUFDN0IsSUFBSSxJQUFBLHVDQUFpQixFQUFDLElBQUEsaUNBQVksR0FBRSxDQUFDLEVBQUU7WUFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLG9CQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdkM7YUFBTSxJQUFJLElBQUEsMkJBQVUsRUFBQyxJQUFBLGlDQUFZLEdBQUUsQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxlQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEM7YUFBTTtZQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQztTQUN2QztJQUNILENBQUM7SUFFRCxxQkFBTSxHQUFOLFVBQU8sSUFBZ0IsRUFBRSxRQUFzQztRQUM3RCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELHFCQUFNLEdBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNILFdBQUM7QUFBRCxDQUFDLEFBcEJELElBb0JDO0FBcEJZLG9CQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU2hhMSBhcyBJZTExU2hhMSB9IGZyb20gXCIuL2llMTFTaGExXCI7XG5pbXBvcnQgeyBTaGExIGFzIFdlYkNyeXB0b1NoYTEgfSBmcm9tIFwiLi93ZWJDcnlwdG9TaGExXCI7XG5pbXBvcnQgeyBIYXNoLCBTb3VyY2VEYXRhIH0gZnJvbSBcIkBhd3Mtc2RrL3R5cGVzXCI7XG5pbXBvcnQgeyBzdXBwb3J0c1dlYkNyeXB0byB9IGZyb20gXCJAYXdzLWNyeXB0by9zdXBwb3J0cy13ZWItY3J5cHRvXCI7XG5pbXBvcnQgeyBpc01zV2luZG93IH0gZnJvbSBcIkBhd3MtY3J5cHRvL2llMTEtZGV0ZWN0aW9uXCI7XG5pbXBvcnQgeyBsb2NhdGVXaW5kb3cgfSBmcm9tIFwiQGF3cy1zZGsvdXRpbC1sb2NhdGUtd2luZG93XCI7XG5cbmV4cG9ydCBjbGFzcyBTaGExIGltcGxlbWVudHMgSGFzaCB7XG4gIHByaXZhdGUgcmVhZG9ubHkgaGFzaDogSGFzaDtcblxuICBjb25zdHJ1Y3RvcihzZWNyZXQ/OiBTb3VyY2VEYXRhKSB7XG4gICAgaWYgKHN1cHBvcnRzV2ViQ3J5cHRvKGxvY2F0ZVdpbmRvdygpKSkge1xuICAgICAgdGhpcy5oYXNoID0gbmV3IFdlYkNyeXB0b1NoYTEoc2VjcmV0KTtcbiAgICB9IGVsc2UgaWYgKGlzTXNXaW5kb3cobG9jYXRlV2luZG93KCkpKSB7XG4gICAgICB0aGlzLmhhc2ggPSBuZXcgSWUxMVNoYTEoc2VjcmV0KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiU0hBMSBub3Qgc3VwcG9ydGVkXCIpO1xuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShkYXRhOiBTb3VyY2VEYXRhLCBlbmNvZGluZz86IFwidXRmOFwiIHwgXCJhc2NpaVwiIHwgXCJsYXRpbjFcIik6IHZvaWQge1xuICAgIHRoaXMuaGFzaC51cGRhdGUoZGF0YSwgZW5jb2RpbmcpO1xuICB9XG5cbiAgZGlnZXN0KCk6IFByb21pc2U8VWludDhBcnJheT4ge1xuICAgIHJldHVybiB0aGlzLmhhc2guZGlnZXN0KCk7XG4gIH1cbn1cbiJdfQ==