"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sha1 = void 0;
var util_utf8_browser_1 = require("@aws-sdk/util-utf8-browser");
var isEmptyData_1 = require("./isEmptyData");
var constants_1 = require("./constants");
var util_locate_window_1 = require("@aws-sdk/util-locate-window");
var Sha1 = /** @class */ (function () {
    function Sha1(secret) {
        this.toHash = new Uint8Array(0);
        if (secret !== void 0) {
            this.key = new Promise(function (resolve, reject) {
                (0, util_locate_window_1.locateWindow)()
                    .crypto.subtle.importKey("raw", convertToBuffer(secret), constants_1.SHA_1_HMAC_ALGO, false, ["sign"])
                    .then(resolve, reject);
            });
            this.key.catch(function () { });
        }
    }
    Sha1.prototype.update = function (data) {
        if ((0, isEmptyData_1.isEmptyData)(data)) {
            return;
        }
        var update = convertToBuffer(data);
        var typedArray = new Uint8Array(this.toHash.byteLength + update.byteLength);
        typedArray.set(this.toHash, 0);
        typedArray.set(update, this.toHash.byteLength);
        this.toHash = typedArray;
    };
    Sha1.prototype.digest = function () {
        var _this = this;
        if (this.key) {
            return this.key.then(function (key) {
                return (0, util_locate_window_1.locateWindow)()
                    .crypto.subtle.sign(constants_1.SHA_1_HMAC_ALGO, key, _this.toHash)
                    .then(function (data) { return new Uint8Array(data); });
            });
        }
        if ((0, isEmptyData_1.isEmptyData)(this.toHash)) {
            return Promise.resolve(constants_1.EMPTY_DATA_SHA_1);
        }
        return Promise.resolve()
            .then(function () { return (0, util_locate_window_1.locateWindow)().crypto.subtle.digest(constants_1.SHA_1_HASH, _this.toHash); })
            .then(function (data) { return Promise.resolve(new Uint8Array(data)); });
    };
    return Sha1;
}());
exports.Sha1 = Sha1;
function convertToBuffer(data) {
    if (typeof data === "string") {
        return (0, util_utf8_browser_1.fromUtf8)(data);
    }
    if (ArrayBuffer.isView(data)) {
        return new Uint8Array(data.buffer, data.byteOffset, data.byteLength / Uint8Array.BYTES_PER_ELEMENT);
    }
    return new Uint8Array(data);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2ViQ3J5cHRvU2hhMS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy93ZWJDcnlwdG9TaGExLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUNBLGdFQUFzRDtBQUN0RCw2Q0FBNEM7QUFDNUMseUNBQTRFO0FBQzVFLGtFQUEyRDtBQUUzRDtJQUlFLGNBQVksTUFBbUI7UUFGdkIsV0FBTSxHQUFlLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRzdDLElBQUksTUFBTSxLQUFLLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtnQkFDckMsSUFBQSxpQ0FBWSxHQUFFO3FCQUNYLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUN0QixLQUFLLEVBQ0wsZUFBZSxDQUFDLE1BQU0sQ0FBQyxFQUN2QiwyQkFBZSxFQUNmLEtBQUssRUFDTCxDQUFDLE1BQU0sQ0FBQyxDQUNUO3FCQUNBLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxjQUFPLENBQUMsQ0FBQyxDQUFDO1NBQzFCO0lBQ0gsQ0FBQztJQUVELHFCQUFNLEdBQU4sVUFBTyxJQUFnQjtRQUNyQixJQUFJLElBQUEseUJBQVcsRUFBQyxJQUFJLENBQUMsRUFBRTtZQUNyQixPQUFPO1NBQ1I7UUFFRCxJQUFNLE1BQU0sR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBTSxVQUFVLEdBQUcsSUFBSSxVQUFVLENBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQzNDLENBQUM7UUFDRixVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDL0IsVUFBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQscUJBQU0sR0FBTjtRQUFBLGlCQWdCQztRQWZDLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNaLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBQyxHQUFHO2dCQUN2QixPQUFBLElBQUEsaUNBQVksR0FBRTtxQkFDWCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywyQkFBZSxFQUFFLEdBQUcsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDO3FCQUNyRCxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBcEIsQ0FBb0IsQ0FBQztZQUZ2QyxDQUV1QyxDQUN4QyxDQUFDO1NBQ0g7UUFFRCxJQUFJLElBQUEseUJBQVcsRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDNUIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLDRCQUFnQixDQUFDLENBQUM7U0FDMUM7UUFFRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUU7YUFDckIsSUFBSSxDQUFDLGNBQU0sT0FBQSxJQUFBLGlDQUFZLEdBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxzQkFBVSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBNUQsQ0FBNEQsQ0FBQzthQUN4RSxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQXJDLENBQXFDLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBQ0gsV0FBQztBQUFELENBQUMsQUFwREQsSUFvREM7QUFwRFksb0JBQUk7QUFzRGpCLFNBQVMsZUFBZSxDQUFDLElBQWdCO0lBQ3ZDLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO1FBQzVCLE9BQU8sSUFBQSw0QkFBUSxFQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3ZCO0lBRUQsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFO1FBQzVCLE9BQU8sSUFBSSxVQUFVLENBQ25CLElBQUksQ0FBQyxNQUFNLEVBQ1gsSUFBSSxDQUFDLFVBQVUsRUFDZixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxpQkFBaUIsQ0FDL0MsQ0FBQztLQUNIO0lBRUQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUM5QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSGFzaCwgU291cmNlRGF0YSB9IGZyb20gXCJAYXdzLXNkay90eXBlc1wiO1xuaW1wb3J0IHsgZnJvbVV0ZjggfSBmcm9tIFwiQGF3cy1zZGsvdXRpbC11dGY4LWJyb3dzZXJcIjtcbmltcG9ydCB7IGlzRW1wdHlEYXRhIH0gZnJvbSBcIi4vaXNFbXB0eURhdGFcIjtcbmltcG9ydCB7IEVNUFRZX0RBVEFfU0hBXzEsIFNIQV8xX0hBU0gsIFNIQV8xX0hNQUNfQUxHTyB9IGZyb20gXCIuL2NvbnN0YW50c1wiO1xuaW1wb3J0IHsgbG9jYXRlV2luZG93IH0gZnJvbSBcIkBhd3Mtc2RrL3V0aWwtbG9jYXRlLXdpbmRvd1wiO1xuXG5leHBvcnQgY2xhc3MgU2hhMSBpbXBsZW1lbnRzIEhhc2gge1xuICBwcml2YXRlIHJlYWRvbmx5IGtleTogUHJvbWlzZTxDcnlwdG9LZXk+IHwgdW5kZWZpbmVkO1xuICBwcml2YXRlIHRvSGFzaDogVWludDhBcnJheSA9IG5ldyBVaW50OEFycmF5KDApO1xuXG4gIGNvbnN0cnVjdG9yKHNlY3JldD86IFNvdXJjZURhdGEpIHtcbiAgICBpZiAoc2VjcmV0ICE9PSB2b2lkIDApIHtcbiAgICAgIHRoaXMua2V5ID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBsb2NhdGVXaW5kb3coKVxuICAgICAgICAgIC5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcbiAgICAgICAgICAgIFwicmF3XCIsXG4gICAgICAgICAgICBjb252ZXJ0VG9CdWZmZXIoc2VjcmV0KSxcbiAgICAgICAgICAgIFNIQV8xX0hNQUNfQUxHTyxcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgW1wic2lnblwiXVxuICAgICAgICAgIClcbiAgICAgICAgICAudGhlbihyZXNvbHZlLCByZWplY3QpO1xuICAgICAgfSk7XG4gICAgICB0aGlzLmtleS5jYXRjaCgoKSA9PiB7fSk7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKGRhdGE6IFNvdXJjZURhdGEpOiB2b2lkIHtcbiAgICBpZiAoaXNFbXB0eURhdGEoZGF0YSkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25zdCB1cGRhdGUgPSBjb252ZXJ0VG9CdWZmZXIoZGF0YSk7XG4gICAgY29uc3QgdHlwZWRBcnJheSA9IG5ldyBVaW50OEFycmF5KFxuICAgICAgdGhpcy50b0hhc2guYnl0ZUxlbmd0aCArIHVwZGF0ZS5ieXRlTGVuZ3RoXG4gICAgKTtcbiAgICB0eXBlZEFycmF5LnNldCh0aGlzLnRvSGFzaCwgMCk7XG4gICAgdHlwZWRBcnJheS5zZXQodXBkYXRlLCB0aGlzLnRvSGFzaC5ieXRlTGVuZ3RoKTtcbiAgICB0aGlzLnRvSGFzaCA9IHR5cGVkQXJyYXk7XG4gIH1cblxuICBkaWdlc3QoKTogUHJvbWlzZTxVaW50OEFycmF5PiB7XG4gICAgaWYgKHRoaXMua2V5KSB7XG4gICAgICByZXR1cm4gdGhpcy5rZXkudGhlbigoa2V5KSA9PlxuICAgICAgICBsb2NhdGVXaW5kb3coKVxuICAgICAgICAgIC5jcnlwdG8uc3VidGxlLnNpZ24oU0hBXzFfSE1BQ19BTEdPLCBrZXksIHRoaXMudG9IYXNoKVxuICAgICAgICAgIC50aGVuKChkYXRhKSA9PiBuZXcgVWludDhBcnJheShkYXRhKSlcbiAgICAgICk7XG4gICAgfVxuXG4gICAgaWYgKGlzRW1wdHlEYXRhKHRoaXMudG9IYXNoKSkge1xuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShFTVBUWV9EQVRBX1NIQV8xKTtcbiAgICB9XG5cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKClcbiAgICAgIC50aGVuKCgpID0+IGxvY2F0ZVdpbmRvdygpLmNyeXB0by5zdWJ0bGUuZGlnZXN0KFNIQV8xX0hBU0gsIHRoaXMudG9IYXNoKSlcbiAgICAgIC50aGVuKChkYXRhKSA9PiBQcm9taXNlLnJlc29sdmUobmV3IFVpbnQ4QXJyYXkoZGF0YSkpKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBjb252ZXJ0VG9CdWZmZXIoZGF0YTogU291cmNlRGF0YSk6IFVpbnQ4QXJyYXkge1xuICBpZiAodHlwZW9mIGRhdGEgPT09IFwic3RyaW5nXCIpIHtcbiAgICByZXR1cm4gZnJvbVV0ZjgoZGF0YSk7XG4gIH1cblxuICBpZiAoQXJyYXlCdWZmZXIuaXNWaWV3KGRhdGEpKSB7XG4gICAgcmV0dXJuIG5ldyBVaW50OEFycmF5KFxuICAgICAgZGF0YS5idWZmZXIsXG4gICAgICBkYXRhLmJ5dGVPZmZzZXQsXG4gICAgICBkYXRhLmJ5dGVMZW5ndGggLyBVaW50OEFycmF5LkJZVEVTX1BFUl9FTEVNRU5UXG4gICAgKTtcbiAgfVxuXG4gIHJldHVybiBuZXcgVWludDhBcnJheShkYXRhKTtcbn1cbiJdfQ==