"use strict";

var _withMessage = _interopRequireDefault(require("./withMessage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

global.expect = (0, _withMessage.default)(global.expect);