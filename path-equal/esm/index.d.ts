/**
 * Compare if the two file paths are considered equal.
 */
export declare function pathEqual(actual: string, expected: string): boolean;
//# sourceMappingURL=index.d.ts.map