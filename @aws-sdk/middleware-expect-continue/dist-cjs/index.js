"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAddExpectContinuePlugin = exports.addExpectContinueMiddlewareOptions = exports.addExpectContinueMiddleware = void 0;
const protocol_http_1 = require("@aws-sdk/protocol-http");
function addExpectContinueMiddleware(options) {
    return (next) => async (args) => {
        const { request } = args;
        if (protocol_http_1.HttpRequest.isInstance(request) && request.body && options.runtime === "node") {
            request.headers = {
                ...request.headers,
                Expect: "100-continue",
            };
        }
        return next({
            ...args,
            request,
        });
    };
}
exports.addExpectContinueMiddleware = addExpectContinueMiddleware;
exports.addExpectContinueMiddlewareOptions = {
    step: "build",
    tags: ["SET_EXPECT_HEADER", "EXPECT_HEADER"],
    name: "addExpectContinueMiddleware",
    override: true,
};
const getAddExpectContinuePlugin = (options) => ({
    applyToStack: (clientStack) => {
        clientStack.add(addExpectContinueMiddleware(options), exports.addExpectContinueMiddlewareOptions);
    },
});
exports.getAddExpectContinuePlugin = getAddExpectContinuePlugin;
