"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.INIT = exports.DIGEST_LENGTH = exports.BLOCK_SIZE = void 0;
exports.BLOCK_SIZE = 64;
exports.DIGEST_LENGTH = 16;
exports.INIT = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476];
