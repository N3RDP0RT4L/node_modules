import { SdkStream } from "@aws-sdk/types";
import { Readable } from "stream";
export declare const sdkStreamMixin: (stream: unknown) => SdkStream<Readable>;
