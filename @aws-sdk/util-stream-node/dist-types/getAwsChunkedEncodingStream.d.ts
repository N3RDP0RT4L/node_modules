/// <reference types="node" />
import { GetAwsChunkedEncodingStream } from "@aws-sdk/types";
import { Readable } from "stream";
export declare const getAwsChunkedEncodingStream: GetAwsChunkedEncodingStream<Readable>;
