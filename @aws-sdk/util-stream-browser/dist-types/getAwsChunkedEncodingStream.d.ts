import { GetAwsChunkedEncodingStream } from "@aws-sdk/types";
export declare const getAwsChunkedEncodingStream: GetAwsChunkedEncodingStream<ReadableStream>;
