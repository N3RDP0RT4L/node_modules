import { SdkStream } from "@aws-sdk/types";
export declare const sdkStreamMixin: (
  stream: unknown
) => SdkStream<ReadableStream | Blob>;
