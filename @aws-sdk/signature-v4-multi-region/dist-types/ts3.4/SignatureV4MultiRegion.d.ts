import { SignatureV4CryptoInit, SignatureV4Init } from "@aws-sdk/signature-v4";
import {
  HttpRequest,
  RequestPresigner,
  RequestPresigningArguments,
  RequestSigner,
  RequestSigningArguments,
} from "@aws-sdk/types";
export declare type SignatureV4MultiRegionInit = SignatureV4Init &
  SignatureV4CryptoInit & {
    runtime?: string;
  };
export declare class SignatureV4MultiRegion
  implements RequestPresigner, RequestSigner
{
  private readonly sigv4Signer;
  private sigv4aSigner?;
  private readonly signerOptions;
  constructor(options: SignatureV4MultiRegionInit);
  sign(
    requestToSign: HttpRequest,
    options?: RequestSigningArguments
  ): Promise<HttpRequest>;
  presign(
    originalRequest: HttpRequest,
    options?: RequestPresigningArguments
  ): Promise<HttpRequest>;
  private getSigv4aSigner;
}
