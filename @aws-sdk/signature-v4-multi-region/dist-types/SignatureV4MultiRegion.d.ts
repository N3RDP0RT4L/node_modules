import { SignatureV4CryptoInit, SignatureV4Init } from "@aws-sdk/signature-v4";
import { HttpRequest, RequestPresigner, RequestPresigningArguments, RequestSigner, RequestSigningArguments } from "@aws-sdk/types";
export declare type SignatureV4MultiRegionInit = SignatureV4Init & SignatureV4CryptoInit & {
    runtime?: string;
};
/**
 * A SigV4-compatible signer for S3 service. In order to support SigV4a algorithm according to the operation input
 * dynamically, the signer wraps native module SigV4a signer and JS SigV4 signer. It signs the request with SigV4a
 * algorithm if the request needs to be signed with `*` region. Otherwise, it signs the request with normal SigV4
 * signer.
 * Note that SigV4a signer is only supported in Node.js now because it depends on a native dependency.
 * @private
 */
export declare class SignatureV4MultiRegion implements RequestPresigner, RequestSigner {
    private readonly sigv4Signer;
    private sigv4aSigner?;
    private readonly signerOptions;
    constructor(options: SignatureV4MultiRegionInit);
    sign(requestToSign: HttpRequest, options?: RequestSigningArguments): Promise<HttpRequest>;
    presign(originalRequest: HttpRequest, options?: RequestPresigningArguments): Promise<HttpRequest>;
    private getSigv4aSigner;
}
