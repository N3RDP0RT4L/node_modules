"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PRIORITY_ORDER_ALGORITHMS = exports.CLIENT_SUPPORTED_ALGORITHMS = void 0;
const constants_1 = require("./constants");
exports.CLIENT_SUPPORTED_ALGORITHMS = [
    constants_1.ChecksumAlgorithm.CRC32,
    constants_1.ChecksumAlgorithm.CRC32C,
    constants_1.ChecksumAlgorithm.SHA1,
    constants_1.ChecksumAlgorithm.SHA256,
];
exports.PRIORITY_ORDER_ALGORITHMS = [
    constants_1.ChecksumAlgorithm.CRC32,
    constants_1.ChecksumAlgorithm.CRC32C,
    constants_1.ChecksumAlgorithm.SHA1,
    constants_1.ChecksumAlgorithm.SHA256,
];
