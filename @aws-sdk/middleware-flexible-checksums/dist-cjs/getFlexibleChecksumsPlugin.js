"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFlexibleChecksumsPlugin = exports.flexibleChecksumsMiddlewareOptions = void 0;
const flexibleChecksumsMiddleware_1 = require("./flexibleChecksumsMiddleware");
exports.flexibleChecksumsMiddlewareOptions = {
    name: "flexibleChecksumsMiddleware",
    step: "build",
    tags: ["BODY_CHECKSUM"],
    override: true,
};
const getFlexibleChecksumsPlugin = (config, middlewareConfig) => ({
    applyToStack: (clientStack) => {
        clientStack.add((0, flexibleChecksumsMiddleware_1.flexibleChecksumsMiddleware)(config, middlewareConfig), exports.flexibleChecksumsMiddlewareOptions);
    },
});
exports.getFlexibleChecksumsPlugin = getFlexibleChecksumsPlugin;
