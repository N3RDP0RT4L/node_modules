"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectChecksumAlgorithmFunction = void 0;
const crc32_1 = require("@aws-crypto/crc32");
const crc32c_1 = require("@aws-crypto/crc32c");
const constants_1 = require("./constants");
const selectChecksumAlgorithmFunction = (checksumAlgorithm, config) => ({
    [constants_1.ChecksumAlgorithm.MD5]: config.md5,
    [constants_1.ChecksumAlgorithm.CRC32]: crc32_1.AwsCrc32,
    [constants_1.ChecksumAlgorithm.CRC32C]: crc32c_1.AwsCrc32c,
    [constants_1.ChecksumAlgorithm.SHA1]: config.sha1,
    [constants_1.ChecksumAlgorithm.SHA256]: config.sha256,
}[checksumAlgorithm]);
exports.selectChecksumAlgorithmFunction = selectChecksumAlgorithmFunction;
