"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChecksum = void 0;
const isStreaming_1 = require("./isStreaming");
const stringHasher_1 = require("./stringHasher");
const getChecksum = async (body, { streamHasher, checksumAlgorithmFn, base64Encoder }) => {
    const digest = (0, isStreaming_1.isStreaming)(body) ? streamHasher(checksumAlgorithmFn, body) : (0, stringHasher_1.stringHasher)(checksumAlgorithmFn, body);
    return base64Encoder(await digest);
};
exports.getChecksum = getChecksum;
