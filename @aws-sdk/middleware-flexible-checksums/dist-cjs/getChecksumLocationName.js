"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChecksumLocationName = void 0;
const constants_1 = require("./constants");
const getChecksumLocationName = (algorithm) => algorithm === constants_1.ChecksumAlgorithm.MD5 ? "content-md5" : `x-amz-checksum-${algorithm.toLowerCase()}`;
exports.getChecksumLocationName = getChecksumLocationName;
