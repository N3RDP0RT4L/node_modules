"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChecksumAlgorithmForRequest = void 0;
const constants_1 = require("./constants");
const types_1 = require("./types");
const getChecksumAlgorithmForRequest = (input, { requestChecksumRequired, requestAlgorithmMember }) => {
    if (!requestAlgorithmMember || !input[requestAlgorithmMember]) {
        return requestChecksumRequired ? constants_1.ChecksumAlgorithm.MD5 : undefined;
    }
    const checksumAlgorithm = input[requestAlgorithmMember];
    if (!types_1.CLIENT_SUPPORTED_ALGORITHMS.includes(checksumAlgorithm)) {
        throw new Error(`The checksum algorithm "${checksumAlgorithm}" is not supported by the client.` +
            ` Select one of ${types_1.CLIENT_SUPPORTED_ALGORITHMS}.`);
    }
    return checksumAlgorithm;
};
exports.getChecksumAlgorithmForRequest = getChecksumAlgorithmForRequest;
