"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChecksumAlgorithmListForResponse = void 0;
const types_1 = require("./types");
const getChecksumAlgorithmListForResponse = (responseAlgorithms = []) => {
    const validChecksumAlgorithms = [];
    for (const algorithm of types_1.PRIORITY_ORDER_ALGORITHMS) {
        if (!responseAlgorithms.includes(algorithm) || !types_1.CLIENT_SUPPORTED_ALGORITHMS.includes(algorithm)) {
            continue;
        }
        validChecksumAlgorithms.push(algorithm);
    }
    return validChecksumAlgorithms;
};
exports.getChecksumAlgorithmListForResponse = getChecksumAlgorithmListForResponse;
