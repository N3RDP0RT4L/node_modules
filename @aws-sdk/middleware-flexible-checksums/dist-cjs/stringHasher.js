"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringHasher = void 0;
const stringHasher = (checksumAlgorithmFn, body) => {
    const hash = new checksumAlgorithmFn();
    hash.update(body || "");
    return hash.digest();
};
exports.stringHasher = stringHasher;
