"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isStreaming = void 0;
const is_array_buffer_1 = require("@aws-sdk/is-array-buffer");
const isStreaming = (body) => body !== undefined && typeof body !== "string" && !ArrayBuffer.isView(body) && !(0, is_array_buffer_1.isArrayBuffer)(body);
exports.isStreaming = isStreaming;
