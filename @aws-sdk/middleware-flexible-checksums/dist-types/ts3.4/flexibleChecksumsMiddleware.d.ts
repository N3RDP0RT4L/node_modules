import { BuildMiddleware } from "@aws-sdk/types";
import { PreviouslyResolved } from "./configuration";
import { FlexibleChecksumsMiddlewareConfig } from "./getFlexibleChecksumsPlugin";
export declare const flexibleChecksumsMiddleware: (
  config: PreviouslyResolved,
  middlewareConfig: FlexibleChecksumsMiddlewareConfig
) => BuildMiddleware<any, any>;
