import { HeaderBag } from "@aws-sdk/types";
export declare const hasHeader: (header: string, headers: HeaderBag) => boolean;
