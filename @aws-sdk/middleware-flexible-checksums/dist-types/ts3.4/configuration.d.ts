import {
  BodyLengthCalculator,
  Encoder,
  GetAwsChunkedEncodingStream,
  HashConstructor,
  StreamHasher,
} from "@aws-sdk/types";
export interface PreviouslyResolved {
  base64Encoder: Encoder;
  bodyLengthChecker: BodyLengthCalculator;
  getAwsChunkedEncodingStream: GetAwsChunkedEncodingStream;
  md5: HashConstructor;
  sha1: HashConstructor;
  sha256: HashConstructor;
  streamHasher: StreamHasher<any>;
}
