import { ChecksumAlgorithm } from "./constants";
export interface GetChecksumAlgorithmForRequestOptions {
  requestChecksumRequired: boolean;
  requestAlgorithmMember?: string;
}
export declare const getChecksumAlgorithmForRequest: (
  input: any,
  {
    requestChecksumRequired,
    requestAlgorithmMember,
  }: GetChecksumAlgorithmForRequestOptions
) => ChecksumAlgorithm | undefined;
