import { HashConstructor } from "@aws-sdk/types";
export declare const stringHasher: (
  checksumAlgorithmFn: HashConstructor,
  body: any
) => Promise<Uint8Array>;
