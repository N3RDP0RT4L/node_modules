import { HashConstructor } from "@aws-sdk/types";
import { PreviouslyResolved } from "./configuration";
import { ChecksumAlgorithm } from "./constants";
export declare const selectChecksumAlgorithmFunction: (
  checksumAlgorithm: ChecksumAlgorithm,
  config: PreviouslyResolved
) => HashConstructor;
