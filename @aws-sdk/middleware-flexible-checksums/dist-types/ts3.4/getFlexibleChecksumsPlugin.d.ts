import { BuildHandlerOptions, Pluggable } from "@aws-sdk/types";
import { PreviouslyResolved } from "./configuration";
export declare const flexibleChecksumsMiddlewareOptions: BuildHandlerOptions;
export interface FlexibleChecksumsMiddlewareConfig {
  input: Object;
  requestChecksumRequired: boolean;
  requestAlgorithmMember?: string;
  requestValidationModeMember?: string;
  responseAlgorithms?: string[];
}
export declare const getFlexibleChecksumsPlugin: (
  config: PreviouslyResolved,
  middlewareConfig: FlexibleChecksumsMiddlewareConfig
) => Pluggable<any, any>;
