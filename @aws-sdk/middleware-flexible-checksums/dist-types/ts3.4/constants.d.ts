export declare enum ChecksumAlgorithm {
  MD5 = "MD5",
  CRC32 = "CRC32",
  CRC32C = "CRC32C",
  SHA1 = "SHA1",
  SHA256 = "SHA256",
}
export declare enum ChecksumLocation {
  HEADER = "header",
  TRAILER = "trailer",
}
