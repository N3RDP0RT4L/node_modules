import { HeaderBag } from "@aws-sdk/types";
/**
 * Returns true if header is present in headers.
 * Comparisons are case-insensitive.
 */
export declare const hasHeader: (header: string, headers: HeaderBag) => boolean;
