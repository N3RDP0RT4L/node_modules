import { Encoder, HashConstructor, StreamHasher } from "@aws-sdk/types";
export interface GetChecksumDigestOptions {
    streamHasher: StreamHasher<any>;
    checksumAlgorithmFn: HashConstructor;
    base64Encoder: Encoder;
}
export declare const getChecksum: (body: unknown, { streamHasher, checksumAlgorithmFn, base64Encoder }: GetChecksumDigestOptions) => Promise<string>;
