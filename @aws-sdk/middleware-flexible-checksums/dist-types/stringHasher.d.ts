import { HashConstructor } from "@aws-sdk/types";
/**
 * A function that, given a hash constructor and a string, calculates the hash of the string.
 */
export declare const stringHasher: (checksumAlgorithmFn: HashConstructor, body: any) => Promise<Uint8Array>;
