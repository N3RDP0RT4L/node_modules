import { WaiterOptions, WaiterResult } from "./waiter";
export declare const createWaiter: <Client, Input>(
  options: WaiterOptions<Client>,
  input: Input,
  acceptorChecks: (client: Client, input: Input) => Promise<WaiterResult>
) => Promise<WaiterResult>;
