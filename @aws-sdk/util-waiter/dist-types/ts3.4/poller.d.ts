import { WaiterOptions, WaiterResult } from "./waiter";
export declare const runPolling: <Client, Input>(
  {
    minDelay,
    maxDelay,
    maxWaitTime,
    abortController,
    client,
    abortSignal,
  }: WaiterOptions<Client>,
  input: Input,
  acceptorChecks: (client: Client, input: Input) => Promise<WaiterResult>
) => Promise<WaiterResult>;
