import { WaiterConfiguration as WaiterConfiguration__ } from "@aws-sdk/types";
export interface WaiterConfiguration<T> extends WaiterConfiguration__<T> {}
export declare const waiterServiceDefaults: {
  minDelay: number;
  maxDelay: number;
};
export declare type WaiterOptions<Client> = WaiterConfiguration<Client> &
  Required<Pick<WaiterConfiguration<Client>, "minDelay" | "maxDelay">>;
export declare enum WaiterState {
  ABORTED = "ABORTED",
  FAILURE = "FAILURE",
  SUCCESS = "SUCCESS",
  RETRY = "RETRY",
  TIMEOUT = "TIMEOUT",
}
export declare type WaiterResult = {
  state: WaiterState;
  reason?: any;
};
export declare const checkExceptions: (result: WaiterResult) => WaiterResult;
