import { WaiterOptions } from "../waiter";
export declare const validateWaiterOptions: <Client>(
  options: WaiterOptions<Client>
) => void;
