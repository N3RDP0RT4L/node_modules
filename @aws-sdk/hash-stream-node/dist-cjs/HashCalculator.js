"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HashCalculator = void 0;
const stream_1 = require("stream");
class HashCalculator extends stream_1.Writable {
    constructor(hash, options) {
        super(options);
        this.hash = hash;
    }
    _write(chunk, encoding, callback) {
        try {
            this.hash.update(chunk);
        }
        catch (err) {
            return callback(err);
        }
        callback();
    }
}
exports.HashCalculator = HashCalculator;
