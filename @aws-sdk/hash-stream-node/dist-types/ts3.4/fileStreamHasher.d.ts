import { StreamHasher } from "@aws-sdk/types";
import { Readable } from "stream";
export declare const fileStreamHasher: StreamHasher<Readable>;
