/// <reference types="node" />
import { StreamHasher } from "@aws-sdk/types";
import { Readable } from "stream";
export declare const readableStreamHasher: StreamHasher<Readable>;
