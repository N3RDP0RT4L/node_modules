"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolveS3Config = void 0;
const resolveS3Config = (input) => {
    var _a, _b, _c;
    return ({
        ...input,
        forcePathStyle: (_a = input.forcePathStyle) !== null && _a !== void 0 ? _a : false,
        useAccelerateEndpoint: (_b = input.useAccelerateEndpoint) !== null && _b !== void 0 ? _b : false,
        disableMultiregionAccessPoints: (_c = input.disableMultiregionAccessPoints) !== null && _c !== void 0 ? _c : false,
    });
};
exports.resolveS3Config = resolveS3Config;
