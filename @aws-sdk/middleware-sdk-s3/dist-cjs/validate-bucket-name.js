"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getValidateBucketNamePlugin = exports.validateBucketNameMiddlewareOptions = exports.validateBucketNameMiddleware = void 0;
const util_arn_parser_1 = require("@aws-sdk/util-arn-parser");
function validateBucketNameMiddleware() {
    return (next) => async (args) => {
        const { input: { Bucket }, } = args;
        if (typeof Bucket === "string" && !(0, util_arn_parser_1.validate)(Bucket) && Bucket.indexOf("/") >= 0) {
            const err = new Error(`Bucket name shouldn't contain '/', received '${Bucket}'`);
            err.name = "InvalidBucketName";
            throw err;
        }
        return next({ ...args });
    };
}
exports.validateBucketNameMiddleware = validateBucketNameMiddleware;
exports.validateBucketNameMiddlewareOptions = {
    step: "initialize",
    tags: ["VALIDATE_BUCKET_NAME"],
    name: "validateBucketNameMiddleware",
    override: true,
};
const getValidateBucketNamePlugin = (unused) => ({
    applyToStack: (clientStack) => {
        clientStack.add(validateBucketNameMiddleware(), exports.validateBucketNameMiddlewareOptions);
    },
});
exports.getValidateBucketNamePlugin = getValidateBucketNamePlugin;
