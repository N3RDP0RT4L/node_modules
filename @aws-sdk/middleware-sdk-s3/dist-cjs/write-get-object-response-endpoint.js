"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getWriteGetObjectResponseEndpointPlugin = exports.writeGetObjectResponseEndpointMiddlewareOptions = exports.writeGetObjectResponseEndpointMiddleware = void 0;
const middleware_bucket_endpoint_1 = require("@aws-sdk/middleware-bucket-endpoint");
const protocol_http_1 = require("@aws-sdk/protocol-http");
const writeGetObjectResponseEndpointMiddleware = (config) => (next, context) => async (args) => {
    const { region: regionProvider, isCustomEndpoint, disableHostPrefix } = config;
    const region = await regionProvider();
    const { request, input } = args;
    if (!protocol_http_1.HttpRequest.isInstance(request))
        return next({ ...args });
    let hostname = request.hostname;
    if (hostname.endsWith("s3.amazonaws.com") || hostname.endsWith("s3-external-1.amazonaws.com")) {
        return next({ ...args });
    }
    if (!isCustomEndpoint) {
        const [, suffix] = (0, middleware_bucket_endpoint_1.getSuffixForArnEndpoint)(request.hostname);
        hostname = `s3-object-lambda.${region}.${suffix}`;
    }
    if (!disableHostPrefix && input.RequestRoute) {
        hostname = `${input.RequestRoute}.${hostname}`;
    }
    request.hostname = hostname;
    context["signing_service"] = "s3-object-lambda";
    if (config.runtime === "node" && !request.headers["content-length"]) {
        request.headers["transfer-encoding"] = "chunked";
    }
    return next({ ...args });
};
exports.writeGetObjectResponseEndpointMiddleware = writeGetObjectResponseEndpointMiddleware;
exports.writeGetObjectResponseEndpointMiddlewareOptions = {
    relation: "after",
    toMiddleware: "contentLengthMiddleware",
    tags: ["WRITE_GET_OBJECT_RESPONSE", "S3", "ENDPOINT"],
    name: "writeGetObjectResponseEndpointMiddleware",
    override: true,
};
const getWriteGetObjectResponseEndpointPlugin = (config) => ({
    applyToStack: (clientStack) => {
        clientStack.addRelativeTo((0, exports.writeGetObjectResponseEndpointMiddleware)(config), exports.writeGetObjectResponseEndpointMiddlewareOptions);
    },
});
exports.getWriteGetObjectResponseEndpointPlugin = getWriteGetObjectResponseEndpointPlugin;
