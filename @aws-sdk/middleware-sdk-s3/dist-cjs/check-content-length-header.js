"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCheckContentLengthHeaderPlugin = exports.checkContentLengthHeaderMiddlewareOptions = exports.checkContentLengthHeader = void 0;
const protocol_http_1 = require("@aws-sdk/protocol-http");
const CONTENT_LENGTH_HEADER = "content-length";
function checkContentLengthHeader() {
    return (next, context) => async (args) => {
        var _a;
        const { request } = args;
        if (protocol_http_1.HttpRequest.isInstance(request)) {
            if (!request.headers[CONTENT_LENGTH_HEADER]) {
                const message = `Are you using a Stream of unknown length as the Body of a PutObject request? Consider using Upload instead from @aws-sdk/lib-storage.`;
                if (typeof ((_a = context === null || context === void 0 ? void 0 : context.logger) === null || _a === void 0 ? void 0 : _a.warn) === "function") {
                    context.logger.warn(message);
                }
                else {
                    console.warn(message);
                }
            }
        }
        return next({ ...args });
    };
}
exports.checkContentLengthHeader = checkContentLengthHeader;
exports.checkContentLengthHeaderMiddlewareOptions = {
    step: "finalizeRequest",
    tags: ["CHECK_CONTENT_LENGTH_HEADER"],
    name: "getCheckContentLengthHeaderPlugin",
    override: true,
};
const getCheckContentLengthHeaderPlugin = (unused) => ({
    applyToStack: (clientStack) => {
        clientStack.add(checkContentLengthHeader(), exports.checkContentLengthHeaderMiddlewareOptions);
    },
});
exports.getCheckContentLengthHeaderPlugin = getCheckContentLengthHeaderPlugin;
