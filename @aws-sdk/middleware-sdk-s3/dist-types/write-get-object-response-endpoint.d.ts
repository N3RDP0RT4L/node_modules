import { BuildMiddleware, Pluggable, Provider, RelativeMiddlewareOptions } from "@aws-sdk/types";
declare type PreviouslyResolved = {
    region: Provider<string>;
    isCustomEndpoint?: boolean;
    disableHostPrefix: boolean;
    runtime: string;
};
/**
 * @internal
 */
export declare const writeGetObjectResponseEndpointMiddleware: (config: PreviouslyResolved) => BuildMiddleware<any, any>;
/**
 * @internal
 */
export declare const writeGetObjectResponseEndpointMiddlewareOptions: RelativeMiddlewareOptions;
/**
 * @internal
 */
export declare const getWriteGetObjectResponseEndpointPlugin: (config: PreviouslyResolved) => Pluggable<any, any>;
export {};
