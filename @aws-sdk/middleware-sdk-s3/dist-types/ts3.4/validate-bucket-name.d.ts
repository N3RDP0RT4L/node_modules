import {
  InitializeHandlerOptions,
  InitializeMiddleware,
  Pluggable,
} from "@aws-sdk/types";
export declare function validateBucketNameMiddleware(): InitializeMiddleware<
  any,
  any
>;
export declare const validateBucketNameMiddlewareOptions: InitializeHandlerOptions;
export declare const getValidateBucketNamePlugin: (
  unused: any
) => Pluggable<any, any>;
