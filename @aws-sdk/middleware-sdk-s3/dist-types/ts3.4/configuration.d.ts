export interface S3InputConfig {
  forcePathStyle?: boolean;
  useAccelerateEndpoint?: boolean;
  disableMultiregionAccessPoints?: boolean;
}
export interface S3ResolvedConfig {
  forcePathStyle: boolean;
  useAccelerateEndpoint: boolean;
  disableMultiregionAccessPoints: boolean;
}
export declare const resolveS3Config: <T>(
  input: T & S3InputConfig
) => T & S3ResolvedConfig;
