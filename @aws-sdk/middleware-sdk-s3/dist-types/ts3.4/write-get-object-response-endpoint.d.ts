import {
  BuildMiddleware,
  Pluggable,
  Provider,
  RelativeMiddlewareOptions,
} from "@aws-sdk/types";
declare type PreviouslyResolved = {
  region: Provider<string>;
  isCustomEndpoint?: boolean;
  disableHostPrefix: boolean;
  runtime: string;
};
export declare const writeGetObjectResponseEndpointMiddleware: (
  config: PreviouslyResolved
) => BuildMiddleware<any, any>;
export declare const writeGetObjectResponseEndpointMiddlewareOptions: RelativeMiddlewareOptions;
export declare const getWriteGetObjectResponseEndpointPlugin: (
  config: PreviouslyResolved
) => Pluggable<any, any>;
export {};
