import { EventStreamMarshaller as UniversalEventStreamMarshaller } from "@aws-sdk/eventstream-serde-universal";
import { Readable } from "stream";
import { readabletoIterable } from "./utils";
export class EventStreamMarshaller {
    constructor({ utf8Encoder, utf8Decoder }) {
        this.universalMarshaller = new UniversalEventStreamMarshaller({
            utf8Decoder,
            utf8Encoder,
        });
    }
    deserialize(body, deserializer) {
        const bodyIterable = typeof body[Symbol.asyncIterator] === "function" ? body : readabletoIterable(body);
        return this.universalMarshaller.deserialize(bodyIterable, deserializer);
    }
    serialize(input, serializer) {
        const serializedIterable = this.universalMarshaller.serialize(input, serializer);
        if (typeof Readable.from === "function") {
            return Readable.from(serializedIterable);
        }
        else {
            const iterator = serializedIterable[Symbol.asyncIterator]();
            const serializedStream = new Readable({
                autoDestroy: true,
                objectMode: true,
                async read() {
                    iterator
                        .next()
                        .then(({ done, value }) => {
                        if (done) {
                            this.push(null);
                        }
                        else {
                            this.push(value);
                        }
                    })
                        .catch((err) => {
                        this.destroy(err);
                    });
                },
            });
            serializedStream.on("error", () => {
                serializedStream.destroy();
            });
            serializedStream.on("end", () => {
                serializedStream.destroy();
            });
            return serializedStream;
        }
    }
}
