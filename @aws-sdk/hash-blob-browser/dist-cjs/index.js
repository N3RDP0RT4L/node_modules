"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blobHasher = void 0;
const chunked_blob_reader_1 = require("@aws-sdk/chunked-blob-reader");
const blobHasher = async function blobHasher(hashCtor, blob) {
    const hash = new hashCtor();
    await (0, chunked_blob_reader_1.blobReader)(blob, (chunk) => {
        hash.update(chunk);
    });
    return hash.digest();
};
exports.blobHasher = blobHasher;
