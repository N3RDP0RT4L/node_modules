import {
  ServiceException as __ServiceException,
  ServiceExceptionOptions as __ServiceExceptionOptions,
} from "@aws-sdk/smithy-client";
export declare class S3ServiceException extends __ServiceException {
  constructor(options: __ServiceExceptionOptions);
}
