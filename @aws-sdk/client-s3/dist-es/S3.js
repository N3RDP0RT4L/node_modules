import { AbortMultipartUploadCommand, } from "./commands/AbortMultipartUploadCommand";
import { CompleteMultipartUploadCommand, } from "./commands/CompleteMultipartUploadCommand";
import { CopyObjectCommand } from "./commands/CopyObjectCommand";
import { CreateBucketCommand, } from "./commands/CreateBucketCommand";
import { CreateMultipartUploadCommand, } from "./commands/CreateMultipartUploadCommand";
import { DeleteBucketAnalyticsConfigurationCommand, } from "./commands/DeleteBucketAnalyticsConfigurationCommand";
import { DeleteBucketCommand, } from "./commands/DeleteBucketCommand";
import { DeleteBucketCorsCommand, } from "./commands/DeleteBucketCorsCommand";
import { DeleteBucketEncryptionCommand, } from "./commands/DeleteBucketEncryptionCommand";
import { DeleteBucketIntelligentTieringConfigurationCommand, } from "./commands/DeleteBucketIntelligentTieringConfigurationCommand";
import { DeleteBucketInventoryConfigurationCommand, } from "./commands/DeleteBucketInventoryConfigurationCommand";
import { DeleteBucketLifecycleCommand, } from "./commands/DeleteBucketLifecycleCommand";
import { DeleteBucketMetricsConfigurationCommand, } from "./commands/DeleteBucketMetricsConfigurationCommand";
import { DeleteBucketOwnershipControlsCommand, } from "./commands/DeleteBucketOwnershipControlsCommand";
import { DeleteBucketPolicyCommand, } from "./commands/DeleteBucketPolicyCommand";
import { DeleteBucketReplicationCommand, } from "./commands/DeleteBucketReplicationCommand";
import { DeleteBucketTaggingCommand, } from "./commands/DeleteBucketTaggingCommand";
import { DeleteBucketWebsiteCommand, } from "./commands/DeleteBucketWebsiteCommand";
import { DeleteObjectCommand, } from "./commands/DeleteObjectCommand";
import { DeleteObjectsCommand, } from "./commands/DeleteObjectsCommand";
import { DeleteObjectTaggingCommand, } from "./commands/DeleteObjectTaggingCommand";
import { DeletePublicAccessBlockCommand, } from "./commands/DeletePublicAccessBlockCommand";
import { GetBucketAccelerateConfigurationCommand, } from "./commands/GetBucketAccelerateConfigurationCommand";
import { GetBucketAclCommand, } from "./commands/GetBucketAclCommand";
import { GetBucketAnalyticsConfigurationCommand, } from "./commands/GetBucketAnalyticsConfigurationCommand";
import { GetBucketCorsCommand, } from "./commands/GetBucketCorsCommand";
import { GetBucketEncryptionCommand, } from "./commands/GetBucketEncryptionCommand";
import { GetBucketIntelligentTieringConfigurationCommand, } from "./commands/GetBucketIntelligentTieringConfigurationCommand";
import { GetBucketInventoryConfigurationCommand, } from "./commands/GetBucketInventoryConfigurationCommand";
import { GetBucketLifecycleConfigurationCommand, } from "./commands/GetBucketLifecycleConfigurationCommand";
import { GetBucketLocationCommand, } from "./commands/GetBucketLocationCommand";
import { GetBucketLoggingCommand, } from "./commands/GetBucketLoggingCommand";
import { GetBucketMetricsConfigurationCommand, } from "./commands/GetBucketMetricsConfigurationCommand";
import { GetBucketNotificationConfigurationCommand, } from "./commands/GetBucketNotificationConfigurationCommand";
import { GetBucketOwnershipControlsCommand, } from "./commands/GetBucketOwnershipControlsCommand";
import { GetBucketPolicyCommand, } from "./commands/GetBucketPolicyCommand";
import { GetBucketPolicyStatusCommand, } from "./commands/GetBucketPolicyStatusCommand";
import { GetBucketReplicationCommand, } from "./commands/GetBucketReplicationCommand";
import { GetBucketRequestPaymentCommand, } from "./commands/GetBucketRequestPaymentCommand";
import { GetBucketTaggingCommand, } from "./commands/GetBucketTaggingCommand";
import { GetBucketVersioningCommand, } from "./commands/GetBucketVersioningCommand";
import { GetBucketWebsiteCommand, } from "./commands/GetBucketWebsiteCommand";
import { GetObjectAclCommand, } from "./commands/GetObjectAclCommand";
import { GetObjectAttributesCommand, } from "./commands/GetObjectAttributesCommand";
import { GetObjectCommand } from "./commands/GetObjectCommand";
import { GetObjectLegalHoldCommand, } from "./commands/GetObjectLegalHoldCommand";
import { GetObjectLockConfigurationCommand, } from "./commands/GetObjectLockConfigurationCommand";
import { GetObjectRetentionCommand, } from "./commands/GetObjectRetentionCommand";
import { GetObjectTaggingCommand, } from "./commands/GetObjectTaggingCommand";
import { GetObjectTorrentCommand, } from "./commands/GetObjectTorrentCommand";
import { GetPublicAccessBlockCommand, } from "./commands/GetPublicAccessBlockCommand";
import { HeadBucketCommand } from "./commands/HeadBucketCommand";
import { HeadObjectCommand } from "./commands/HeadObjectCommand";
import { ListBucketAnalyticsConfigurationsCommand, } from "./commands/ListBucketAnalyticsConfigurationsCommand";
import { ListBucketIntelligentTieringConfigurationsCommand, } from "./commands/ListBucketIntelligentTieringConfigurationsCommand";
import { ListBucketInventoryConfigurationsCommand, } from "./commands/ListBucketInventoryConfigurationsCommand";
import { ListBucketMetricsConfigurationsCommand, } from "./commands/ListBucketMetricsConfigurationsCommand";
import { ListBucketsCommand } from "./commands/ListBucketsCommand";
import { ListMultipartUploadsCommand, } from "./commands/ListMultipartUploadsCommand";
import { ListObjectsCommand } from "./commands/ListObjectsCommand";
import { ListObjectsV2Command, } from "./commands/ListObjectsV2Command";
import { ListObjectVersionsCommand, } from "./commands/ListObjectVersionsCommand";
import { ListPartsCommand } from "./commands/ListPartsCommand";
import { PutBucketAccelerateConfigurationCommand, } from "./commands/PutBucketAccelerateConfigurationCommand";
import { PutBucketAclCommand, } from "./commands/PutBucketAclCommand";
import { PutBucketAnalyticsConfigurationCommand, } from "./commands/PutBucketAnalyticsConfigurationCommand";
import { PutBucketCorsCommand, } from "./commands/PutBucketCorsCommand";
import { PutBucketEncryptionCommand, } from "./commands/PutBucketEncryptionCommand";
import { PutBucketIntelligentTieringConfigurationCommand, } from "./commands/PutBucketIntelligentTieringConfigurationCommand";
import { PutBucketInventoryConfigurationCommand, } from "./commands/PutBucketInventoryConfigurationCommand";
import { PutBucketLifecycleConfigurationCommand, } from "./commands/PutBucketLifecycleConfigurationCommand";
import { PutBucketLoggingCommand, } from "./commands/PutBucketLoggingCommand";
import { PutBucketMetricsConfigurationCommand, } from "./commands/PutBucketMetricsConfigurationCommand";
import { PutBucketNotificationConfigurationCommand, } from "./commands/PutBucketNotificationConfigurationCommand";
import { PutBucketOwnershipControlsCommand, } from "./commands/PutBucketOwnershipControlsCommand";
import { PutBucketPolicyCommand, } from "./commands/PutBucketPolicyCommand";
import { PutBucketReplicationCommand, } from "./commands/PutBucketReplicationCommand";
import { PutBucketRequestPaymentCommand, } from "./commands/PutBucketRequestPaymentCommand";
import { PutBucketTaggingCommand, } from "./commands/PutBucketTaggingCommand";
import { PutBucketVersioningCommand, } from "./commands/PutBucketVersioningCommand";
import { PutBucketWebsiteCommand, } from "./commands/PutBucketWebsiteCommand";
import { PutObjectAclCommand, } from "./commands/PutObjectAclCommand";
import { PutObjectCommand } from "./commands/PutObjectCommand";
import { PutObjectLegalHoldCommand, } from "./commands/PutObjectLegalHoldCommand";
import { PutObjectLockConfigurationCommand, } from "./commands/PutObjectLockConfigurationCommand";
import { PutObjectRetentionCommand, } from "./commands/PutObjectRetentionCommand";
import { PutObjectTaggingCommand, } from "./commands/PutObjectTaggingCommand";
import { PutPublicAccessBlockCommand, } from "./commands/PutPublicAccessBlockCommand";
import { RestoreObjectCommand, } from "./commands/RestoreObjectCommand";
import { SelectObjectContentCommand, } from "./commands/SelectObjectContentCommand";
import { UploadPartCommand } from "./commands/UploadPartCommand";
import { UploadPartCopyCommand, } from "./commands/UploadPartCopyCommand";
import { WriteGetObjectResponseCommand, } from "./commands/WriteGetObjectResponseCommand";
import { S3Client } from "./S3Client";
export class S3 extends S3Client {
    abortMultipartUpload(args, optionsOrCb, cb) {
        const command = new AbortMultipartUploadCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    completeMultipartUpload(args, optionsOrCb, cb) {
        const command = new CompleteMultipartUploadCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    copyObject(args, optionsOrCb, cb) {
        const command = new CopyObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    createBucket(args, optionsOrCb, cb) {
        const command = new CreateBucketCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    createMultipartUpload(args, optionsOrCb, cb) {
        const command = new CreateMultipartUploadCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucket(args, optionsOrCb, cb) {
        const command = new DeleteBucketCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketAnalyticsConfiguration(args, optionsOrCb, cb) {
        const command = new DeleteBucketAnalyticsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketCors(args, optionsOrCb, cb) {
        const command = new DeleteBucketCorsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketEncryption(args, optionsOrCb, cb) {
        const command = new DeleteBucketEncryptionCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketIntelligentTieringConfiguration(args, optionsOrCb, cb) {
        const command = new DeleteBucketIntelligentTieringConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketInventoryConfiguration(args, optionsOrCb, cb) {
        const command = new DeleteBucketInventoryConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketLifecycle(args, optionsOrCb, cb) {
        const command = new DeleteBucketLifecycleCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketMetricsConfiguration(args, optionsOrCb, cb) {
        const command = new DeleteBucketMetricsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketOwnershipControls(args, optionsOrCb, cb) {
        const command = new DeleteBucketOwnershipControlsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketPolicy(args, optionsOrCb, cb) {
        const command = new DeleteBucketPolicyCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketReplication(args, optionsOrCb, cb) {
        const command = new DeleteBucketReplicationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketTagging(args, optionsOrCb, cb) {
        const command = new DeleteBucketTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteBucketWebsite(args, optionsOrCb, cb) {
        const command = new DeleteBucketWebsiteCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteObject(args, optionsOrCb, cb) {
        const command = new DeleteObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteObjects(args, optionsOrCb, cb) {
        const command = new DeleteObjectsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deleteObjectTagging(args, optionsOrCb, cb) {
        const command = new DeleteObjectTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    deletePublicAccessBlock(args, optionsOrCb, cb) {
        const command = new DeletePublicAccessBlockCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketAccelerateConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketAccelerateConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketAcl(args, optionsOrCb, cb) {
        const command = new GetBucketAclCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketAnalyticsConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketAnalyticsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketCors(args, optionsOrCb, cb) {
        const command = new GetBucketCorsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketEncryption(args, optionsOrCb, cb) {
        const command = new GetBucketEncryptionCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketIntelligentTieringConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketIntelligentTieringConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketInventoryConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketInventoryConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketLifecycleConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketLifecycleConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketLocation(args, optionsOrCb, cb) {
        const command = new GetBucketLocationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketLogging(args, optionsOrCb, cb) {
        const command = new GetBucketLoggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketMetricsConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketMetricsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketNotificationConfiguration(args, optionsOrCb, cb) {
        const command = new GetBucketNotificationConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketOwnershipControls(args, optionsOrCb, cb) {
        const command = new GetBucketOwnershipControlsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketPolicy(args, optionsOrCb, cb) {
        const command = new GetBucketPolicyCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketPolicyStatus(args, optionsOrCb, cb) {
        const command = new GetBucketPolicyStatusCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketReplication(args, optionsOrCb, cb) {
        const command = new GetBucketReplicationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketRequestPayment(args, optionsOrCb, cb) {
        const command = new GetBucketRequestPaymentCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketTagging(args, optionsOrCb, cb) {
        const command = new GetBucketTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketVersioning(args, optionsOrCb, cb) {
        const command = new GetBucketVersioningCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getBucketWebsite(args, optionsOrCb, cb) {
        const command = new GetBucketWebsiteCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObject(args, optionsOrCb, cb) {
        const command = new GetObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectAcl(args, optionsOrCb, cb) {
        const command = new GetObjectAclCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectAttributes(args, optionsOrCb, cb) {
        const command = new GetObjectAttributesCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectLegalHold(args, optionsOrCb, cb) {
        const command = new GetObjectLegalHoldCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectLockConfiguration(args, optionsOrCb, cb) {
        const command = new GetObjectLockConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectRetention(args, optionsOrCb, cb) {
        const command = new GetObjectRetentionCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectTagging(args, optionsOrCb, cb) {
        const command = new GetObjectTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getObjectTorrent(args, optionsOrCb, cb) {
        const command = new GetObjectTorrentCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    getPublicAccessBlock(args, optionsOrCb, cb) {
        const command = new GetPublicAccessBlockCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    headBucket(args, optionsOrCb, cb) {
        const command = new HeadBucketCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    headObject(args, optionsOrCb, cb) {
        const command = new HeadObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listBucketAnalyticsConfigurations(args, optionsOrCb, cb) {
        const command = new ListBucketAnalyticsConfigurationsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listBucketIntelligentTieringConfigurations(args, optionsOrCb, cb) {
        const command = new ListBucketIntelligentTieringConfigurationsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listBucketInventoryConfigurations(args, optionsOrCb, cb) {
        const command = new ListBucketInventoryConfigurationsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listBucketMetricsConfigurations(args, optionsOrCb, cb) {
        const command = new ListBucketMetricsConfigurationsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listBuckets(args, optionsOrCb, cb) {
        const command = new ListBucketsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listMultipartUploads(args, optionsOrCb, cb) {
        const command = new ListMultipartUploadsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listObjects(args, optionsOrCb, cb) {
        const command = new ListObjectsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listObjectsV2(args, optionsOrCb, cb) {
        const command = new ListObjectsV2Command(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listObjectVersions(args, optionsOrCb, cb) {
        const command = new ListObjectVersionsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    listParts(args, optionsOrCb, cb) {
        const command = new ListPartsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketAccelerateConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketAccelerateConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketAcl(args, optionsOrCb, cb) {
        const command = new PutBucketAclCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketAnalyticsConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketAnalyticsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketCors(args, optionsOrCb, cb) {
        const command = new PutBucketCorsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketEncryption(args, optionsOrCb, cb) {
        const command = new PutBucketEncryptionCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketIntelligentTieringConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketIntelligentTieringConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketInventoryConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketInventoryConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketLifecycleConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketLifecycleConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketLogging(args, optionsOrCb, cb) {
        const command = new PutBucketLoggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketMetricsConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketMetricsConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketNotificationConfiguration(args, optionsOrCb, cb) {
        const command = new PutBucketNotificationConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketOwnershipControls(args, optionsOrCb, cb) {
        const command = new PutBucketOwnershipControlsCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketPolicy(args, optionsOrCb, cb) {
        const command = new PutBucketPolicyCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketReplication(args, optionsOrCb, cb) {
        const command = new PutBucketReplicationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketRequestPayment(args, optionsOrCb, cb) {
        const command = new PutBucketRequestPaymentCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketTagging(args, optionsOrCb, cb) {
        const command = new PutBucketTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketVersioning(args, optionsOrCb, cb) {
        const command = new PutBucketVersioningCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putBucketWebsite(args, optionsOrCb, cb) {
        const command = new PutBucketWebsiteCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObject(args, optionsOrCb, cb) {
        const command = new PutObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObjectAcl(args, optionsOrCb, cb) {
        const command = new PutObjectAclCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObjectLegalHold(args, optionsOrCb, cb) {
        const command = new PutObjectLegalHoldCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObjectLockConfiguration(args, optionsOrCb, cb) {
        const command = new PutObjectLockConfigurationCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObjectRetention(args, optionsOrCb, cb) {
        const command = new PutObjectRetentionCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putObjectTagging(args, optionsOrCb, cb) {
        const command = new PutObjectTaggingCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    putPublicAccessBlock(args, optionsOrCb, cb) {
        const command = new PutPublicAccessBlockCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    restoreObject(args, optionsOrCb, cb) {
        const command = new RestoreObjectCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    selectObjectContent(args, optionsOrCb, cb) {
        const command = new SelectObjectContentCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    uploadPart(args, optionsOrCb, cb) {
        const command = new UploadPartCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    uploadPartCopy(args, optionsOrCb, cb) {
        const command = new UploadPartCopyCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
    writeGetObjectResponse(args, optionsOrCb, cb) {
        const command = new WriteGetObjectResponseCommand(args);
        if (typeof optionsOrCb === "function") {
            this.send(command, optionsOrCb);
        }
        else if (typeof cb === "function") {
            if (typeof optionsOrCb !== "object")
                throw new Error(`Expect http options but get ${typeof optionsOrCb}`);
            this.send(command, optionsOrCb || {}, cb);
        }
        else {
            return this.send(command, optionsOrCb);
        }
    }
}
