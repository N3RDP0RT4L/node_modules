"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRuntimeConfig = void 0;
const tslib_1 = require("tslib");
const package_json_1 = tslib_1.__importDefault(require("../package.json"));
const sha1_browser_1 = require("@aws-crypto/sha1-browser");
const sha256_browser_1 = require("@aws-crypto/sha256-browser");
const config_resolver_1 = require("@aws-sdk/config-resolver");
const eventstream_serde_browser_1 = require("@aws-sdk/eventstream-serde-browser");
const fetch_http_handler_1 = require("@aws-sdk/fetch-http-handler");
const hash_blob_browser_1 = require("@aws-sdk/hash-blob-browser");
const invalid_dependency_1 = require("@aws-sdk/invalid-dependency");
const md5_js_1 = require("@aws-sdk/md5-js");
const middleware_retry_1 = require("@aws-sdk/middleware-retry");
const util_body_length_browser_1 = require("@aws-sdk/util-body-length-browser");
const util_stream_browser_1 = require("@aws-sdk/util-stream-browser");
const util_user_agent_browser_1 = require("@aws-sdk/util-user-agent-browser");
const util_utf8_browser_1 = require("@aws-sdk/util-utf8-browser");
const runtimeConfig_shared_1 = require("./runtimeConfig.shared");
const smithy_client_1 = require("@aws-sdk/smithy-client");
const util_defaults_mode_browser_1 = require("@aws-sdk/util-defaults-mode-browser");
const getRuntimeConfig = (config) => {
    const defaultsMode = (0, util_defaults_mode_browser_1.resolveDefaultsModeConfig)(config);
    const defaultConfigProvider = () => defaultsMode().then(smithy_client_1.loadConfigsForDefaultMode);
    const clientSharedValues = (0, runtimeConfig_shared_1.getRuntimeConfig)(config);
    return {
        ...clientSharedValues,
        ...config,
        runtime: "browser",
        defaultsMode,
        bodyLengthChecker: config?.bodyLengthChecker ?? util_body_length_browser_1.calculateBodyLength,
        credentialDefaultProvider: config?.credentialDefaultProvider ?? ((_) => () => Promise.reject(new Error("Credential is missing"))),
        defaultUserAgentProvider: config?.defaultUserAgentProvider ??
            (0, util_user_agent_browser_1.defaultUserAgent)({ serviceId: clientSharedValues.serviceId, clientVersion: package_json_1.default.version }),
        eventStreamSerdeProvider: config?.eventStreamSerdeProvider ?? eventstream_serde_browser_1.eventStreamSerdeProvider,
        getAwsChunkedEncodingStream: config?.getAwsChunkedEncodingStream ?? util_stream_browser_1.getAwsChunkedEncodingStream,
        maxAttempts: config?.maxAttempts ?? middleware_retry_1.DEFAULT_MAX_ATTEMPTS,
        md5: config?.md5 ?? md5_js_1.Md5,
        region: config?.region ?? (0, invalid_dependency_1.invalidProvider)("Region is missing"),
        requestHandler: config?.requestHandler ?? new fetch_http_handler_1.FetchHttpHandler(defaultConfigProvider),
        retryMode: config?.retryMode ?? (async () => (await defaultConfigProvider()).retryMode || middleware_retry_1.DEFAULT_RETRY_MODE),
        sdkStreamMixin: config?.sdkStreamMixin ?? util_stream_browser_1.sdkStreamMixin,
        sha1: config?.sha1 ?? sha1_browser_1.Sha1,
        sha256: config?.sha256 ?? sha256_browser_1.Sha256,
        streamCollector: config?.streamCollector ?? fetch_http_handler_1.streamCollector,
        streamHasher: config?.streamHasher ?? hash_blob_browser_1.blobHasher,
        useDualstackEndpoint: config?.useDualstackEndpoint ?? (() => Promise.resolve(config_resolver_1.DEFAULT_USE_DUALSTACK_ENDPOINT)),
        useFipsEndpoint: config?.useFipsEndpoint ?? (() => Promise.resolve(config_resolver_1.DEFAULT_USE_FIPS_ENDPOINT)),
        utf8Decoder: config?.utf8Decoder ?? util_utf8_browser_1.fromUtf8,
        utf8Encoder: config?.utf8Encoder ?? util_utf8_browser_1.toUtf8,
    };
};
exports.getRuntimeConfig = getRuntimeConfig;
