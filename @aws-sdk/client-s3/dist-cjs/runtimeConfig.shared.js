"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRuntimeConfig = void 0;
const signature_v4_multi_region_1 = require("@aws-sdk/signature-v4-multi-region");
const smithy_client_1 = require("@aws-sdk/smithy-client");
const url_parser_1 = require("@aws-sdk/url-parser");
const util_base64_1 = require("@aws-sdk/util-base64");
const endpointResolver_1 = require("./endpoint/endpointResolver");
const getRuntimeConfig = (config) => ({
    apiVersion: "2006-03-01",
    base64Decoder: config?.base64Decoder ?? util_base64_1.fromBase64,
    base64Encoder: config?.base64Encoder ?? util_base64_1.toBase64,
    disableHostPrefix: config?.disableHostPrefix ?? false,
    endpointProvider: config?.endpointProvider ?? endpointResolver_1.defaultEndpointResolver,
    logger: config?.logger ?? new smithy_client_1.NoOpLogger(),
    serviceId: config?.serviceId ?? "S3",
    signerConstructor: config?.signerConstructor ?? signature_v4_multi_region_1.SignatureV4MultiRegion,
    signingEscapePath: config?.signingEscapePath ?? false,
    urlParser: config?.urlParser ?? url_parser_1.parseUrl,
    useArnRegion: config?.useArnRegion ?? false,
});
exports.getRuntimeConfig = getRuntimeConfig;
