"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NODE_USE_ARN_REGION_CONFIG_OPTIONS = exports.NODE_USE_ARN_REGION_INI_NAME = exports.NODE_USE_ARN_REGION_ENV_NAME = void 0;
const util_config_provider_1 = require("@aws-sdk/util-config-provider");
exports.NODE_USE_ARN_REGION_ENV_NAME = "AWS_S3_USE_ARN_REGION";
exports.NODE_USE_ARN_REGION_INI_NAME = "s3_use_arn_region";
exports.NODE_USE_ARN_REGION_CONFIG_OPTIONS = {
    environmentVariableSelector: (env) => (0, util_config_provider_1.booleanSelector)(env, exports.NODE_USE_ARN_REGION_ENV_NAME, util_config_provider_1.SelectorType.ENV),
    configFileSelector: (profile) => (0, util_config_provider_1.booleanSelector)(profile, exports.NODE_USE_ARN_REGION_INI_NAME, util_config_provider_1.SelectorType.CONFIG),
    default: false,
};
