"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bucketHostname = void 0;
const bucketHostnameUtils_1 = require("./bucketHostnameUtils");
const bucketHostname = (options) => {
    (0, bucketHostnameUtils_1.validateCustomEndpoint)(options);
    return (0, bucketHostnameUtils_1.isBucketNameOptions)(options)
        ?
            getEndpointFromBucketName(options)
        :
            getEndpointFromArn(options);
};
exports.bucketHostname = bucketHostname;
const getEndpointFromBucketName = ({ accelerateEndpoint = false, clientRegion: region, baseHostname, bucketName, dualstackEndpoint = false, fipsEndpoint = false, pathStyleEndpoint = false, tlsCompatible = true, isCustomEndpoint = false, }) => {
    const [clientRegion, hostnameSuffix] = isCustomEndpoint ? [region, baseHostname] : (0, bucketHostnameUtils_1.getSuffix)(baseHostname);
    if (pathStyleEndpoint || !(0, bucketHostnameUtils_1.isDnsCompatibleBucketName)(bucketName) || (tlsCompatible && bucketHostnameUtils_1.DOT_PATTERN.test(bucketName))) {
        return {
            bucketEndpoint: false,
            hostname: dualstackEndpoint ? `s3.dualstack.${clientRegion}.${hostnameSuffix}` : baseHostname,
        };
    }
    if (accelerateEndpoint) {
        baseHostname = `s3-accelerate${dualstackEndpoint ? ".dualstack" : ""}.${hostnameSuffix}`;
    }
    else if (dualstackEndpoint) {
        baseHostname = `s3.dualstack.${clientRegion}.${hostnameSuffix}`;
    }
    return {
        bucketEndpoint: true,
        hostname: `${bucketName}.${baseHostname}`,
    };
};
const getEndpointFromArn = (options) => {
    const { isCustomEndpoint, baseHostname, clientRegion } = options;
    const hostnameSuffix = isCustomEndpoint ? baseHostname : (0, bucketHostnameUtils_1.getSuffixForArnEndpoint)(baseHostname)[1];
    const { pathStyleEndpoint, accelerateEndpoint = false, fipsEndpoint = false, tlsCompatible = true, bucketName, clientPartition = "aws", } = options;
    (0, bucketHostnameUtils_1.validateArnEndpointOptions)({ pathStyleEndpoint, accelerateEndpoint, tlsCompatible });
    const { service, partition, accountId, region, resource } = bucketName;
    (0, bucketHostnameUtils_1.validateService)(service);
    (0, bucketHostnameUtils_1.validatePartition)(partition, { clientPartition });
    (0, bucketHostnameUtils_1.validateAccountId)(accountId);
    const { accesspointName, outpostId } = (0, bucketHostnameUtils_1.getArnResources)(resource);
    if (service === "s3-object-lambda") {
        return getEndpointFromObjectLambdaArn({ ...options, tlsCompatible, bucketName, accesspointName, hostnameSuffix });
    }
    if (region === "") {
        return getEndpointFromMRAPArn({ ...options, clientRegion, mrapAlias: accesspointName, hostnameSuffix });
    }
    if (outpostId) {
        return getEndpointFromOutpostArn({ ...options, clientRegion, outpostId, accesspointName, hostnameSuffix });
    }
    return getEndpointFromAccessPointArn({ ...options, clientRegion, accesspointName, hostnameSuffix });
};
const getEndpointFromObjectLambdaArn = ({ dualstackEndpoint = false, fipsEndpoint = false, tlsCompatible = true, useArnRegion, clientRegion, clientSigningRegion = clientRegion, accesspointName, bucketName, hostnameSuffix, }) => {
    const { accountId, region, service } = bucketName;
    (0, bucketHostnameUtils_1.validateRegionalClient)(clientRegion);
    (0, bucketHostnameUtils_1.validateRegion)(region, {
        useArnRegion,
        clientRegion,
        clientSigningRegion,
        allowFipsRegion: true,
        useFipsEndpoint: fipsEndpoint,
    });
    (0, bucketHostnameUtils_1.validateNoDualstack)(dualstackEndpoint);
    const DNSHostLabel = `${accesspointName}-${accountId}`;
    (0, bucketHostnameUtils_1.validateDNSHostLabel)(DNSHostLabel, { tlsCompatible });
    const endpointRegion = useArnRegion ? region : clientRegion;
    const signingRegion = useArnRegion ? region : clientSigningRegion;
    return {
        bucketEndpoint: true,
        hostname: `${DNSHostLabel}.${service}${fipsEndpoint ? "-fips" : ""}.${endpointRegion}.${hostnameSuffix}`,
        signingRegion,
        signingService: service,
    };
};
const getEndpointFromMRAPArn = ({ disableMultiregionAccessPoints, dualstackEndpoint = false, isCustomEndpoint, mrapAlias, hostnameSuffix, }) => {
    if (disableMultiregionAccessPoints === true) {
        throw new Error("SDK is attempting to use a MRAP ARN. Please enable to feature.");
    }
    (0, bucketHostnameUtils_1.validateMrapAlias)(mrapAlias);
    (0, bucketHostnameUtils_1.validateNoDualstack)(dualstackEndpoint);
    return {
        bucketEndpoint: true,
        hostname: `${mrapAlias}${isCustomEndpoint ? "" : `.accesspoint.s3-global`}.${hostnameSuffix}`,
        signingRegion: "*",
    };
};
const getEndpointFromOutpostArn = ({ useArnRegion, clientRegion, clientSigningRegion = clientRegion, bucketName, outpostId, dualstackEndpoint = false, fipsEndpoint = false, tlsCompatible = true, accesspointName, isCustomEndpoint, hostnameSuffix, }) => {
    (0, bucketHostnameUtils_1.validateRegionalClient)(clientRegion);
    (0, bucketHostnameUtils_1.validateRegion)(bucketName.region, { useArnRegion, clientRegion, clientSigningRegion, useFipsEndpoint: fipsEndpoint });
    const DNSHostLabel = `${accesspointName}-${bucketName.accountId}`;
    (0, bucketHostnameUtils_1.validateDNSHostLabel)(DNSHostLabel, { tlsCompatible });
    const endpointRegion = useArnRegion ? bucketName.region : clientRegion;
    const signingRegion = useArnRegion ? bucketName.region : clientSigningRegion;
    (0, bucketHostnameUtils_1.validateOutpostService)(bucketName.service);
    (0, bucketHostnameUtils_1.validateDNSHostLabel)(outpostId, { tlsCompatible });
    (0, bucketHostnameUtils_1.validateNoDualstack)(dualstackEndpoint);
    (0, bucketHostnameUtils_1.validateNoFIPS)(fipsEndpoint);
    const hostnamePrefix = `${DNSHostLabel}.${outpostId}`;
    return {
        bucketEndpoint: true,
        hostname: `${hostnamePrefix}${isCustomEndpoint ? "" : `.s3-outposts.${endpointRegion}`}.${hostnameSuffix}`,
        signingRegion,
        signingService: "s3-outposts",
    };
};
const getEndpointFromAccessPointArn = ({ useArnRegion, clientRegion, clientSigningRegion = clientRegion, bucketName, dualstackEndpoint = false, fipsEndpoint = false, tlsCompatible = true, accesspointName, isCustomEndpoint, hostnameSuffix, }) => {
    (0, bucketHostnameUtils_1.validateRegionalClient)(clientRegion);
    (0, bucketHostnameUtils_1.validateRegion)(bucketName.region, {
        useArnRegion,
        clientRegion,
        clientSigningRegion,
        allowFipsRegion: true,
        useFipsEndpoint: fipsEndpoint,
    });
    const hostnamePrefix = `${accesspointName}-${bucketName.accountId}`;
    (0, bucketHostnameUtils_1.validateDNSHostLabel)(hostnamePrefix, { tlsCompatible });
    const endpointRegion = useArnRegion ? bucketName.region : clientRegion;
    const signingRegion = useArnRegion ? bucketName.region : clientSigningRegion;
    (0, bucketHostnameUtils_1.validateS3Service)(bucketName.service);
    return {
        bucketEndpoint: true,
        hostname: `${hostnamePrefix}${isCustomEndpoint
            ? ""
            : `.s3-accesspoint${fipsEndpoint ? "-fips" : ""}${dualstackEndpoint ? ".dualstack" : ""}.${endpointRegion}`}.${hostnameSuffix}`,
        signingRegion,
    };
};
