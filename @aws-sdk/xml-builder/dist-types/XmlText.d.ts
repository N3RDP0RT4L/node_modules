import { Stringable } from "./stringable";
/**
 * Represents an XML text value.
 */
export declare class XmlText implements Stringable {
    private value;
    constructor(value: string);
    toString(): string;
}
