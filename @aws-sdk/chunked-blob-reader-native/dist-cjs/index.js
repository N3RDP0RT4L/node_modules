"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blobReader = void 0;
const util_base64_1 = require("@aws-sdk/util-base64");
function blobReader(blob, onChunk, chunkSize = 1024 * 1024) {
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.onerror = reject;
        fileReader.onabort = reject;
        const size = blob.size;
        let totalBytesRead = 0;
        const read = () => {
            if (totalBytesRead >= size) {
                resolve();
                return;
            }
            fileReader.readAsDataURL(blob.slice(totalBytesRead, Math.min(size, totalBytesRead + chunkSize)));
        };
        fileReader.onload = (event) => {
            const result = event.target.result;
            const dataOffset = result.indexOf(",") + 1;
            const data = result.substring(dataOffset);
            const decoded = (0, util_base64_1.fromBase64)(data);
            onChunk(decoded);
            totalBytesRead += decoded.byteLength;
            read();
        };
        read();
    });
}
exports.blobReader = blobReader;
