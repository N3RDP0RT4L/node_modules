"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLocationConstraintPlugin = exports.locationConstraintMiddlewareOptions = exports.locationConstraintMiddleware = void 0;
function locationConstraintMiddleware(options) {
    return (next) => async (args) => {
        const { CreateBucketConfiguration } = args.input;
        const region = await options.region();
        if (!CreateBucketConfiguration || !CreateBucketConfiguration.LocationConstraint) {
            args = {
                ...args,
                input: {
                    ...args.input,
                    CreateBucketConfiguration: region === "us-east-1" ? undefined : { LocationConstraint: region },
                },
            };
        }
        return next(args);
    };
}
exports.locationConstraintMiddleware = locationConstraintMiddleware;
exports.locationConstraintMiddlewareOptions = {
    step: "initialize",
    tags: ["LOCATION_CONSTRAINT", "CREATE_BUCKET_CONFIGURATION"],
    name: "locationConstraintMiddleware",
    override: true,
};
const getLocationConstraintPlugin = (config) => ({
    applyToStack: (clientStack) => {
        clientStack.add(locationConstraintMiddleware(config), exports.locationConstraintMiddlewareOptions);
    },
});
exports.getLocationConstraintPlugin = getLocationConstraintPlugin;
